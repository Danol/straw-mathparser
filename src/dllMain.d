﻿module dllMain;/*
version( DLL ):

import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.string;
import straw.mathParser.env;
import straw.mathParser.grammar.entry;
import straw.mathParser.value;
import straw.parser;

alias CString = const( char )*;

extern( C++ )
interface DLLInterface {

public:
	CString getResult();
	CString getError();
	CString getErrorArg();

	size_t getErrorPos();
	size_t getErrorPosLength();

public:
	void setMaxNesting( uint set );
	void setUseFractions( bool set );
	void setDenominatorLimit( uint set );
	void setAngleMode( int set );

public:
	void parse( CString expr, bool intermediate, uint timeout );
	void recalculate( bool intermediate );

	// Does not parse anything, just 'redisplays' last result - useful when display settings has changed
	void redisplay();

}

private DLLInterface iface;

export extern( C )
DLLInterface getInterface() {
	if( !iface )
		iface = new DLLInterfaceImpl;

	return iface;
}

final class DLLInterfaceImpl : DLLInterface {

private:
	Environment env;
	Parser parser;
	Values lastResultValues;
	Symbol_Entry lastParsed;

	CString resultStr, errorStr, errorArgStr;
	size_t errorPos, errorPosLength;

	Value.Sink resultBuffer;

public:
	this() {
		env = new Environment();
		parser = new Parser();

		resultBuffer = appender!( char[] )();
	}

	extern( C++ ) {

	public:
		override CString getResult() {
			return resultStr;
		}
		override CString getError() {
			return errorStr;
		}
		override CString getErrorArg() {
			return errorArgStr;
		}

		override size_t getErrorPos() {
			return errorPos;
		}
		override size_t getErrorPosLength() {
			return errorPosLength;
		}

	public:
		void setMaxNesting( uint set ) {
			env.maxNesting = set;
		}
		void setUseFractions( bool set ) {
			env.useFractions = set;
		}
		void setDenominatorLimit( uint set ) {
			env.denominatorLimit = set;
		}
		void setAngleMode( int set ) {
			env.angleMode = cast( AngleMode ) set;
		}

	public:
		override void parse( CString expr, bool intermediate, uint timeout ) {
			// Create temporary namespace if the mode is intermediate
				
			errorStr = "".toStringz;
			errorArgStr = "".toStringz;

			errorPos = -1;

			env.timeout = timeout;
			env.start();

			try {
				parser.reuse( expr.to!string );
				lastParsed = parser.parse!Symbol_Entry();

				enforce( lastParsed, new SyntaxException( "unexpectedSymbol", PositionData( 0, 0, 0, parser.sourceLen ) ) );
				enforce( parser.getChar == 0, new SyntaxException( "unexpectedSymbol", PositionData( parser.posData, parser.sourceLen - parser.pos ) ) );

				recalculate( intermediate );
				redisplay();
			}
			catch( ParserException e ) {
				errorStr = e.msg.toStringz;
				errorArgStr = e.arg.toStringz;

				errorPos = e.posData.posInFile;
				errorPosLength = e.posData.length;

				lastParsed = null;
			}
			catch( Throwable e ) {
				errorStr = ( "##EXCEPTION##:" ~ e.msg ).toStringz;
				errorPos = 0;
			}
		}
		override void recalculate( bool intermediate ) {
			if( intermediate )
				env.namespaceStack ~= Namespace();

			try 
				lastResultValues = lastParsed.calculate( env );

			catch( ParserException e ) {
				errorStr = e.msg.toStringz;
				errorArgStr = e.arg.toStringz;
				
				errorPos = e.posData.posInFile;
				errorPosLength = e.posData.length;

				lastResultValues = null;
			}
			catch( Throwable e ) {
				errorStr = ( "##EXCEPTION##:" ~ e.msg ).toStringz;
				errorPos = 0;
			}

			if( intermediate )
				env.namespaceStack.length --;
			
			else if( lastResultValues.length )
				env.defVariable( "ans", lastResultValues[ $-1 ], PositionData() );
		}
		override void redisplay() {
			resultBuffer.clear();

			if( lastResultValues.length ) {
				lastResultValues[ 0 ].toString( env, resultBuffer );

				foreach( val; lastResultValues[ 1 .. $ ] ) {
					resultBuffer ~= ", ";
					val.toString( env, resultBuffer );
				}
			}

			resultBuffer ~= cast( char ) 0;
			resultStr = resultBuffer.data.ptr;
		}

	}

}*/