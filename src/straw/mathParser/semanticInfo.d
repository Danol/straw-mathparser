﻿module straw.mathParser.semanticInfo;

enum SemanticInfoType {
	identifier
}

extern( C++ )
	interface SemanticInfo_CppInterface {

public:
	SemanticInfoType getType();

}

abstract class SemanticInfo : SemanticInfo_CppInterface {

public:
	immutable SemanticInfoType type;

public:
	this( SemanticInfoType type ) {
		this.type = type;
	}

public:
	extern( C++ )
	override SemanticInfoType getType() {
		return type;
	}

}

