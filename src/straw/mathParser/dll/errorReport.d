﻿module straw.mathParser.dll.errorReport;

import std.string;
import straw.mathParser.dll.interface_;
import straw.parser.parser;

extern( C++ )
	interface ICppParserErrorReport
{

public:
	CString c_str();
	CString c_arg();
	
	size_t c_pos();
	size_t c_length();

}

final class ParserErrorReport : ICppParserErrorReport {

private:
	CString cstr, carg;
	size_t pos, len;

public:
	this( string str, string arg, PositionData posData ) {
		cstr = str.toStringz;
		carg = arg.toStringz;

		pos = posData.posInFile;
		len = posData.length;
	}

public:
	extern( C++ ) override {
		CString c_str() {
			return cstr;
		}
		CString c_arg() {
			return carg;
		}
		
		size_t c_pos() {
			return pos;
		}
		size_t c_length() {
			return len;
		}
	}

}