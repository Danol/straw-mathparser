﻿module straw.mathParser.dll.result;

import std.string;
import straw.mathParser.dll.errorReport;
import straw.mathParser.dll.interface_;
import straw.mathParser.grammar.entry;
import straw.mathParser.value;

extern( C++ )
	interface ICppParserResult
{

public:
	enum Type {
		standard,
		intermediate,
		redisplay,
		recalculate
	}

public:
	void c_drop();

public:
	CString c_input();
	CString c_result();

	Type c_type();
	ICppParserErrorReport c_error();

}

final class ParserResult : ICppParserResult {

public:
	ParserErrorReport err;
	string input, result;
	CString cinput, cresult;
	Symbol_Entry parsed;
	Values vals;
	Type type;
	size_t posInList;

public:
	this( string input, string result, Type type, Symbol_Entry parsed, Values vals ) {
		cinput = input.toStringz;
		cresult = result.toStringz;

		this.input = input;
		this.result = result;
		this.parsed = parsed;
		this.vals = vals;
		this.type = type;

		posInList = cppInterface.resultList.add( this );
		cppInterface.lastResult = this;
	}
	this( string input, ParserErrorReport err, Type type ) {
		cinput = input.toStringz;
		cresult = "".toStringz;

		this.err = err;
		this.type = type;

		posInList = cppInterface.resultList.add( this );
		cppInterface.lastResult = this;
	}

public:
	extern( C++ ) override {
		void c_drop() {
			cppInterface.resultList.removeIndex( posInList );
		}
	}

public:
	extern( C++ ) override {
		CString c_input() {
			return cinput;
		}
		CString c_result() {
			return cresult;
		}

		Type c_type() {
			return type;
		}
		ICppParserErrorReport c_error() {
			return err;
		}
	}

}