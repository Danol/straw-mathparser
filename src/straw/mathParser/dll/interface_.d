﻿module straw.mathParser.dll.interface_;

import core.thread;
import std.array;
import std.conv;
import std.concurrency;
import std.exception;
import std.typecons;
import straw.core.storage.container;
import straw.mathParser.dll.result;
import straw.mathParser.dll.settings;
import straw.mathParser.env;
import straw.mathParser.value;
import straw.mathParser.grammar.entry;
import straw.mathParser.dll.errorReport;
import straw.parser;

alias CString = const( char )*;

extern( C++ )
	interface ICppInterface
{

public:
	alias ErrorCallback = void function( CString );

public:
	void c_stop();
	ICppInterfaceSettings c_settings();

	void c_setErrorCallback( ErrorCallback set );
	ICppParserResult c_getFirstUndroppedResult();

public:
	void c_parse( CString input, int timeout );
	void c_parseIntermediate( CString input, int timeout );
	/// Always intermediate
	void c_reparse( int timeout );
	void c_redisplay();

}

final class CppInterface : ICppInterface {

public:
	enum Action {
		none,
		parse,
		parseIntermediate,
		redisplay,
		recalculate
	}

public:
	Environment env;
	Parser parser;
	Container!( ParserResult, ContainerPolicy.nullBasedWithCache ) resultList;
	ParserResult lastResult;

private:
	CppInterfaceSettings settings;
	ErrorCallback errorCallback;
	Thread thrd;
	Action action;
	string input;
	int inputTimeout;
	bool shouldRun;

private:
	Value.Sink resultBuffer;

public:
	this() {
		thrd = new Thread( &func );
		env = new Environment();
		parser = new Parser();
		settings = new CppInterfaceSettings( this );

		resultBuffer = appender!( char[] )();

		shouldRun = true;

		thrd.start();
	}

private:
	void func() {
		while( true ) {
			Action tmpAct;
			string tmpInput;

			synchronized( this ) {
				tmpAct = action;
				tmpInput = input;
				env.timeout = inputTimeout;

				action = Action.none;
			}

			ParserResult result;

			try if( tmpInput.length ) final switch( tmpAct ) {

				case Action.none:
					break;

				case Action.parse:
					auto res = parse( tmpInput, false );
					result = new ParserResult( tmpInput, res[ 0 ], ParserResult.Type.standard, res[ 1 ], res[ 2 ] );
					break;

				case Action.parseIntermediate:
					auto res = parse( tmpInput, true );
					result = new ParserResult( tmpInput, res[ 0 ], ParserResult.Type.intermediate, res[ 1 ], res[ 2 ] );
					break;

				case Action.redisplay:
					if( lastResult ) {
						string res = evaluate( lastResult.vals );
						result = new ParserResult( lastResult.input, res, ParserResult.Type.redisplay, lastResult.parsed, lastResult.vals );
					}
					break;

				case Action.recalculate:
					if( lastResult ) {
						Values vals = calculate( lastResult.parsed, true );
						string res = evaluate( vals );
						result = new ParserResult( lastResult.input, res, ParserResult.Type.recalculate, lastResult.parsed, vals );
					}
					break;

			}
			catch( ParserException exc ) {
				result = new ParserResult( tmpInput, new ParserErrorReport( exc.msg, exc.arg, exc.posData ), cast( ParserResult.Type )( cast( int ) tmpAct - 1 ) );
			}
			catch( Exception exc ) {
				result = new ParserResult( tmpInput, new ParserErrorReport( "##EXCEPTION## " ~ exc.msg, "", PositionData() ), cast( ParserResult.Type )( cast( int ) tmpAct - 1 ) );
			}

			synchronized( this )
				if( !shouldRun ) break;

			Thread.sleep( 50.dur!"msecs" );
		}
	}

private:
	auto parse( string expr, bool intermediate ) {
		env.start();

		parser.reuse( expr.to!string );
		Symbol_Entry parsed = parser.parse!Symbol_Entry();
		
		enforce( parsed, new SyntaxException( "unexpectedSymbol", PositionData( 0, 0, 0, parser.sourceLen ) ) );
		enforce( parser.getChar == 0, new SyntaxException( "unexpectedSymbol", PositionData( parser.posData, parser.sourceLen - parser.pos ) ) );
		
		Values result = calculate( parsed, intermediate );
		return tuple( evaluate( result ), parsed, result );
	}
	Values calculate( Symbol_Entry parsed, bool intermediate ) {
		if( intermediate )
			env.namespaceStack ~= Namespace();

		Values result = parsed.calculate( env );

		if( intermediate )
			env.namespaceStack.length --;

		else if( result.length )
			env.defVariable( "ans", result[ $-1 ], PositionData() );

		return result;
	}
	string evaluate( Values vals ) {
		resultBuffer.clear();
		
		if( vals.length ) {
			vals[ 0 ].toString( env, resultBuffer );
			
			foreach( val; vals[ 1 .. $ ] ) {
				resultBuffer ~= ", ";
				val.toString( env, resultBuffer );
			}
		}

		return cast( string ) resultBuffer.data.dup;
	}

public:
	extern( C++ ) override {
		void c_stop() {
			synchronized( this )
				shouldRun = false;
		}
		ICppInterfaceSettings c_settings() {
			return settings;
		}

		void c_setErrorCallback( ErrorCallback set ) {
			errorCallback = set;
		}
		ICppParserResult c_getFirstUndroppedResult() {
			synchronized( this )
				foreach( res; resultList )
					return res;

			return null;
		}
	}

public:
	extern( C++ ) override {
		void c_parse( CString input, int timeout ) {
			synchronized( this ) {
				action = Action.parse;
				this.input = input.to!string;
				inputTimeout = timeout;
			}
		}
		void c_parseIntermediate( CString input, int timeout ) {
			synchronized( this ) {
				if( action > Action.parseIntermediate ) {
					errorCallback( "parseIntermediateFail" );
					return;
				}

				action = Action.parseIntermediate;
				this.input = input.to!string;
				inputTimeout = timeout;
			}
		}
		void c_reparse( int timeout ) {
			synchronized( this ) {
				if( action > Action.recalculate ) {
					errorCallback( "reparseFail" );
					return;
				}
				
				action = Action.recalculate;
				inputTimeout = timeout;
				// input stays
			}
		}
		void c_redisplay() {
			synchronized( this ) {
				if( action > Action.redisplay ) {
					errorCallback( "redisplayFail" );
					return;
				}
				
				action = Action.redisplay;
				// input stays
				// ditoo inputTimeout
			}
		}
	}

}

__gshared CppInterface cppInterface;

export extern( C )
ICppInterface getInterface() {
	if( !cppInterface )
		cppInterface = new CppInterface;
	
	return cppInterface;
}