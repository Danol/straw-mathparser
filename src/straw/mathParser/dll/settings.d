﻿module straw.mathParser.dll.settings;

import straw.mathParser.dll.interface_;
import straw.mathParser.env;

extern( C++ )
	interface ICppInterfaceSettings
{
	
public:
	void c_maxNesting( uint set );
	void c_useFractions( bool set );
	void c_denominatorLimit( uint set );
	void c_angleMode( int set );
	
}

final class CppInterfaceSettings : ICppInterfaceSettings {
	
private:
	CppInterface i;
	
public:
	this( CppInterface i ) {
		this.i = i;
	}
	
public:
	extern( C++ ) override {
		void c_maxNesting( uint set ) {
			synchronized( this )
				i.env.maxNesting = set;
		}
		void c_useFractions( bool set ) {
			synchronized( this )
				i.env.useFractions = set;
		}
		void c_denominatorLimit( uint set ) {
			synchronized( this )
				i.env.denominatorLimit = set;
		}
		void c_angleMode( int set ) {
			synchronized( this )
				i.env.angleMode = cast( AngleMode ) set;
		}
	}
	
}