﻿module straw.mathParser.value.realv;

import std.algorithm;
import std.complex;
import std.conv;
import std.exception;
import std.format;
import std.math;
import std.typecons;
import straw.mathParser.env;
import straw.mathParser.value;

Value realValue( real value ) {
	return new RealValue( value );
}

final class RealValue : Value {
	mixin Value_mix!( "real", realValue, 0, ComplexValue, AnyNumericalValue );

public:
	real value;
	alias value this;

public:
	this( real value ) {
		this.value = value;
	}

public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {

			case ComplexValue.sname:
				return new ComplexValue( complex( value ) );

			case AnyNumericalValue.sname:
				return new AnyNumericalValue( this );

			default:
				assert( 0 );

		}
	}
	
public:
	override void toString( Environment env, ref Sink sink ) {
		value.toString( env, sink );
	}

}

private struct RealConstants_t {

public:
	RealValue zero = new RealValue( 0 );

public:
	RealValue one = new RealValue( 1 );

public:
	RealValue e = new RealValue( E );
	RealValue pi = new RealValue( PI );

public:
	RealValue ln2 = new RealValue( LN2 );
	RealValue ln10 = new RealValue( LN10 );

}
enum RealConstants = RealConstants_t();

Tuple!( ulong, ulong ) realToFraction( Environment env, real x ) {
	// http://en.wikipedia.org/wiki/Generalized_continued_fraction#Fundamental_recurrence_formulas

	if( x == 0 )
		return tuple( cast( ulong ) 0, cast( ulong ) 1 );

	x = abs( x );

	ulong numerator_pre = 1;
	ulong numerator = cast( ulong ) x.floor;

	ulong denominator_pre = 0;
	ulong denominator = 1;
	ulong denominator_limit = env.denominatorLimit;

	ulong b = numerator;

	ulong tmp;
	real xTmp = x;

	while(
		abs( xTmp - b ) > double.epsilon &&
		abs( cast( real ) numerator / denominator - x ) > double.epsilon
		) {

		xTmp = 1 / ( xTmp - b );
		b = cast( ulong ) xTmp.floor;

		tmp = numerator;
		numerator = numerator * b + numerator_pre;
		numerator_pre = tmp;

		tmp = denominator;
		denominator = denominator * b + denominator_pre;
		denominator_pre = tmp;

		if( denominator > denominator_limit )
			return tuple( cast( ulong ) 0, cast( ulong ) -1 );
	}

	return tuple( numerator, denominator );
}

private struct FRM {

public:
	real function() value;
	string name;

}
enum FractionMultipliers = [
	FRM( () => 1, 					"" 		),
	FRM( () => sqrt( 2.0 ), "√2" 	),
	FRM( () => sqrt( 3.0 ), "√3" 	),
	FRM( () => PI, 					"π" 	),
];

void toString( real value, Environment env, ref Value.Sink sink ) {
	if( !env.useFractions || value == value.floor ) {
		formattedWrite( sink, "%s", value );
		return;
	}

	ulong numerator, denominator = -1;
	string multiplier;

	foreach( mult; FractionMultipliers ) {
		auto frac = env.realToFraction( value / mult.value() );

		if( frac[ 1 ] < denominator ) {
			numerator = frac[ 0 ];
			denominator = frac[ 1 ];
			multiplier = mult.name;
		}
	}

	if( denominator == -1 ) {
		formattedWrite( sink, "%s", value );
		return;
	}

	if( value < 0 )
		sink ~= "-";

	if( numerator != 1 || multiplier == "" )
		formattedWrite( sink, "%s", numerator );

	sink ~= multiplier;

	if( denominator != 1 )
		formattedWrite( sink, "/%s", denominator );
}