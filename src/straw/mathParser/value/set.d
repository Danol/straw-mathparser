﻿module straw.mathParser.value.set;

import std.utf;
import std.exception;
import straw.mathParser.env;
import straw.mathParser.value;

GeneralSetValue generalSetValue( Value[] values ) {
	return new GeneralSetValue( values );
}
final class GeneralSetValue : Value {
	mixin Value_mix!( "generalSet", generalSetValue, 3 );

public:
	Value[] values;

private:
	string[] castables;

public:
	this( Value[] values ) {
		this.values = values;

		if( values.length > 0 ) {
			castables = values[ 0 ].castableTo;

			foreach( val; values[ 1 .. $ ] )
				castables = setIntersection( castables, val.castableTo ).array();

			castables = castables.map!( c => "specializedSet_" ~ c ).array();
		}
	}

public:
	override Value castToImpl( string targetName ) {
		foreach( castable; castables ) {
			auto constructor = specializedSetValueConstructors.get( castable, null );
			enforce( constructor, "I made some stupid mistake probably. And yeah, I am aware that this error description doesn't actually say anything." );

		}

		assert( 0 );
	}

public:
	override void toString( Environment env, ref Sink sink ) {
		sink ~= "{";

		if( values.length ) {
			values[ 0 ].toString( env, sink );

			foreach( val; values[ 1 .. $ ] )
				sink ~= ", ", val.toString( env, sink );
		}

		sink ~= "}";
	}

}


alias SpecializedSetValueConstructor = Value[] function( Value[] );
SpecializedSetValueConstructor[ string ] specializedSetValueConstructors;

SpecializedSetValue specializedSetValue( T : Value )( T[] values ) {
	return new SpecializedSetValue!T( values );
}
final class SpecializedSetValue( T : Value ) : Value {
	mixin Value_mix!( "specializedSet_" ~ T.sname, specializedSetValue, 3, GeneralSetValue );

public:
	enum t_sname = T.sname;

public:
	T[] values;
	
public:
	this( T[] values ) {
		this.values = values;
	}
	
public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {
			
			case GeneralSetValue.sname:
				return generalSetValue( values.map!( v => cast( Value ) v ).array );
				
			default:
				assert( 0 );
				
		}
	}
	
public:
	override string toString( Environment env ) {
		return "{ " ~ values.map!( v => v.toString( env ) ).joiner( ", " ).array ~ " }";
	}

private:
	shared static this() {
		specializedSetValueConstructors[ sname ] = ( vals ) {
			return new typeof( this )( vals.map!( v => cast( T ) v.castTo( sname ) ).array );
		};
	}
	
}