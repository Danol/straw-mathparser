﻿module straw.mathParser.env.func;

import straw.mathParser.env.env;
import straw.mathParser.value;
import straw.parser.parser;

class Function {
	
public:
	immutable string[] argumentTypeNames;
	immutable string file;
	immutable int line;
	
public:
	this( string[] argumentTypeNames, string file, int line ) {
		this.argumentTypeNames = cast( immutable string[] ) argumentTypeNames;
		this.file = file;
		this.line = line;
	}

public:
	/// Returns count of casts that would be required to call this overload or -1 if it is not possible at all with these values
	abstract Casts castsRequired( Values vals );

	abstract Value call( Environment env, ref PositionData posData, Values values );

}