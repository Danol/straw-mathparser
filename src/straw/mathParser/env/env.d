﻿module straw.mathParser.env.env;

import std.algorithm;
import std.datetime;
import std.exception;
import std.math;
import straw.core.utils.typetuple;
import straw.mathParser.env.func;
import straw.mathParser.env.systemFunc;
import straw.mathParser.exception;
import straw.mathParser.value;
import straw.parser.parser;

bool castAssign( T, S )( ref T var, S val )
	if( is( S == class ) && is( T : S )  )
{
	var = cast( T ) val;
	return var !is null;
}

struct Namespace {

public:
	Function[][ string ] functions;
	Value[ string ] variables;

}

enum AngleMode {
	deg,
	rad,
	perc
}

class Environment {

public:
	static Function[][ string ] systemFunctions;
	static Value[ string ] systemVariables;

public:
	/// Maximum time (in ms) allowed for evaluation
	uint timeout = -1;

	/// Maximum function call nesting
	uint maxNesting = -1;
	bool useFractions = true;
	AngleMode angleMode = AngleMode.rad;
	ulong denominatorLimit = 1000;

public:
	uint currentNesting = 0;

public:
	Namespace[] namespaceStack;

private:
	StopWatch timeoutWatch;

public:
	this() {
		// System namespace
		namespaceStack ~= Namespace( systemFunctions, systemVariables );

		// User namespace
		namespaceStack ~= Namespace();
	}

public:
	/// Creates a static (system-defined) function. Call this in shared static this()
	static void newSystemFunction( Args ... )( string name, Value function( Args args ) func, string file = __FILE__, int line = __LINE__ ) {
		enforce( name !in systemVariables, new SemanticException( "cannotOverloadIdentifier", PositionData(), name ) );
		systemFunctions[ name ] = systemFunctions.get( name, null ) ~ new SystemFunction!Args( func, file, line );
	}

	/// Creates a static (system-defined) variable. Call this in shared static this()
	static void newSystemVariable( string name, Value val ) {
		enforce( name !in systemFunctions && name !in systemVariables, new SemanticException( "cannotOverloadIdentifier", PositionData(), name ) );
		systemVariables[ name ] = val;
	}

public:
	/// Creates/updates a user-defined variable. Cannot overload system variables
	void defVariable( string name, Value val, PositionData posData ) {
		enforce( !isSystemVariable( name ) && !isFunction( name ), new SemanticException( "cannotOverloadIdentifier", PositionData(), name ) );
		namespaceStack[ $ - 1 ].variables[ name ] = val;
	}

public:
	bool isIdentifier( string name ) {
		return namespaceStack.any!( n => ( name in n.functions ) || ( name in n.variables ) );
	}
	bool isFunction( string name ) {
		return namespaceStack.any!( n => ( name in n.functions ) !is null );
	}
	bool isVariable( string name ) {
		return namespaceStack.any!( n => ( name in n.variables ) !is null );
	}

	bool isSystemIdentifier( string name ) {
		return isSystemFunction( name ) || isSystemVariable( name );
	}
	bool isSystemFunction( string name ) {
		return systemFunctions.get( name, null ) !is null;
	}
	bool isSystemVariable( string name ) {
		return systemVariables.get( name, null ) !is null;
	}

public:
	Value calculateIdentifier( string name, Values args, PositionData posData ) {
		checkTimeout( posData );

		// Look for variables
		foreach_reverse( ref namespace; namespaceStack ) {
			if( Value res = namespace.variables.get( name, null ) ) {
				enforce( args.length == 0, new SemanticException( "identifierIsAVariableNotAFunction", posData, name ) );
				return res;
			}
		}

		bool functionExists = false;
		Casts minCasts, casts;
		Function minCastsFunc = null;

		foreach_reverse( ref namespace; namespaceStack ) {
			if( Function[] overloads = namespace.functions.get( name, null ) ) {
				functionExists = true;

				foreach( func; overloads ) {
					casts = func.castsRequired( args );

					/*import std.stdio;
					import std.algorithm;
					import std.array;
					import std.conv;
					writeln( "  test of " ~ name ~ " (" ~ func.file ~ ":" ~ func.line.to!string ~ ") ", casts );
					writeln( "    ", args.map!( a => a.name ).joiner( ", " ).array );*/

					if( casts == errorCasts ) {

					} else if( minCastsFunc is null ) {
						minCasts = casts;
						minCastsFunc = func;

					} else {
						foreach( a; 0 .. MaxValueLevel ) {
							if( casts[ a ] > minCasts[ a ] ) {
								minCasts = casts;
								minCastsFunc = func;

							} else if ( casts[ a ] < minCasts[ a ] ) {
								break;
							}
						}
					}
				}
			}
		}

		if( minCastsFunc ) {
			/*import std.stdio;
			import std.conv;
			writeln( "overload of " ~ name ~ " (" ~ minCastsFunc.file ~ ":" ~ minCastsFunc.line.to!string ~ ")" );*/

			return minCastsFunc.call( this, posData, args );
		}

		if( functionExists )
			throw new SemanticException( "matchingOverloadNotFound", posData, name );

		else
			throw new SemanticException( "undefinedIdentifier", posData, name );
	}

public:
	void start() {
		timeoutWatch.start();
	}

	/// Checks if calculation time is greater than timeout, throws an exception in that case
	void checkTimeout( PositionData posData ) {
		if( timeoutWatch.peek().msecs > timeout )
			throw new EvaluationException( "evaluationTimeout", posData );
	}

public:
	real envToRad( real val ) {
		final switch( angleMode ) {

			case AngleMode.deg:
				return val / 180.0 * PI;

			case AngleMode.rad:
				return val;

			case AngleMode.perc:
				return val * 2.0 * PI;

		}
	}

	Value radToEnv( real val ) {
		final switch( angleMode ) {

			case AngleMode.deg:
				return realValue( val / PI * 180.0 );

			case AngleMode.rad:
				return realValue( val );

			case AngleMode.perc:
				return realValue( val * 2 * PI );

		}
	}

}