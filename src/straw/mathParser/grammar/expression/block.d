﻿module straw.mathParser.grammar.expression.block;

import straw.mathParser.grammar.toolkit;
import straw.mathParser.grammar.expression.binaryop;

final class Symbol_Block : Symbol {
	
public:
	enum name = "block";
	
public:
	@required
	Symbol_Terminal!( "leftBracket", "(" ) leftBracket;

	@optional
	Symbol_Space space1;

	@required @errorChecked( "expression" )
	Symbol_Expression expr;

	@optional
	Symbol_Space space2;

	@required @errorChecked( "rightBracket" )
	Symbol_Terminal!( "rightBracket", ")" ) rightBracket;
	
public:
	string debugStr() {
		return expr.debugStr;
	}
	Value calculate( Environment env ) {
		return expr.calculate( env );
	}
	SemanticInfo[] getSemanticInfo( size_t posInFile ) {
		if( expr.posData.hasInRange( posInFile ) )
			return expr.getSemanticInfo( posInFile );

		return null;
	}
	
}