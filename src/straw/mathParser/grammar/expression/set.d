﻿module straw.mathParser.grammar.expression.set;

import straw.mathParser.grammar.toolkit;

final class Symbol_Set : Symbol {

public:
	enum name = "set";
	
public:
	@required
	Symbol_Terminal!( "leftCurlyBracket", "{" ) leftBracketTerminal;
	
	@optional
	Symbol_Space space1;
	
	@optional
	Items items;
	
	@optional
	Symbol_Space space2;
	
	@required @errorChecked( "rightCurlyBracket" )
	Symbol_Terminal!( "rightCurlyBracket", "}" ) rightBracketTerminal;
	
public:
	string debugStr() {
		return "{ " ~ ( items ? items.item1.debugStr ~ items.otherItems.map!( a => ", " ~ a.expr.debugStr ).array.join : "" ) ~ " }";
	}
	Value calculate( Environment env ) {
		return generalSetValue( items ? items.calculate( env ) : null );
	}
	SemanticInfo[] getSemanticInfo( size_t posInFile ) {
		if( items.posData.hasInRange( posInFile ) )
			return items.getSemanticInfo( posInFile );
		
		return null;
	}
	
private:
	final static class Items : Symbol {
		
	public:
		enum name = "set_items1+";
		
	public:
		@required
		Symbol_Expression item1;
		
		@optionalRepeated
		OtherArg[] otherItems;

		Values calculate( Environment env ) {
			return [ item1.calculate( env ) ] ~ otherItems.map!( a => a.expr.calculate( env ) ).array;
		}
		SemanticInfo[] getSemanticInfo( size_t posInFile ) {
			if( item1.posData.hasInRange( posInFile ) )
				return item1.getSemanticInfo( posInFile );
			
			foreach( item; otherItems ) {
				if( item.posData.hasInRange( posInFile ) )
					return item.expr.posData.hasInRange( posInFile ) ? item.expr.getSemanticInfo( posInFile ) : null;
			}
			
			return null;
		}
		
	}
	
	final static class OtherArg : Symbol {
		
	public:
		enum name = "set_items2+";
		
	public:
		@optional
		Symbol_Space space1;
		
		@required
		Symbol_Terminal!( "comma", "," ) commaTerminal;
		
		@optional
		Symbol_Space space2;
		
		@required @errorChecked( "expression" )
		Symbol_Expression expr;
		
	}
	
}