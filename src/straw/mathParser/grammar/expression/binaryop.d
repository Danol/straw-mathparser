﻿module straw.mathParser.grammar.expression.binaryop;

import straw.mathParser.grammar.expression.atomic;
import straw.mathParser.grammar.toolkit;

alias Symbol_PlusMinusExpression = Symbol_BinaryOperatorExpression!( "plusMinusExpression", [ "+", "-" ], Symbol_MulDivExpression );
alias Symbol_MulDivExpression = Symbol_BinaryOperatorExpression!( "mulDivExpression", [ "*", "/" ], Symbol_PowerExpression );
alias Symbol_PowerExpression = Symbol_BinaryOperatorExpression!( "powerExpression", [ "^" ], Symbol_Atomic );

class Symbol_BinaryOperatorExpression( string name_, string[] operators, LowerPriority ) : Symbol {
	static assert( is( LowerPriority : Symbol ) );

public:
	enum name = name_;

public:
	@required
	LowerPriority left;

	@optionalRepeated
	ExprRight[] rights;
	
public:
	string debugStr() {
		return ( rights ? "( " : "" ) ~ left.debugStr ~ rights.map!( r => " op" ~ r.operator.text ~ " " ~ r.expr.debugStr ).array.join ~ ( rights ? " ) " : "" );
	}
	/// Calls function op[X]( left, right ), nested in case of multiple rights
	Value calculate( Environment env ) {
		if( !rights )
			return left.calculate( env );

		Value result = left.calculate( env );
		foreach( right; rights )
			result = env.calculateIdentifier( "#op" ~ right.operator.text, [ result, right.expr.calculate( env ) ], posData );

		return result;
	}
	 SemanticInfo[] getSemanticInfo( size_t posInFile ) {
		if( left.posData.hasInRange( posInFile ) )
			return left.getSemanticInfo( posInFile );
		
		foreach( right; rights ) {
			if( right.posData.hasInRange( posInFile ) )
				return right.expr.posData.hasInRange( posInFile ) ? right.expr.getSemanticInfo( posInFile ) : null;
		}
		
		return null;
	}

private:
	final static class ExprRight : Symbol {
		
	public:
		enum name = name_ ~ "_right";
		
	public:
		@optional
		Symbol_Space space1;
		
		@required
		Symbol_TerminalVariants!( name_ ~ "_operators", operators ) operator;
		
		@optional
		Symbol_Space space2;
		
		@required @errorChecked( "rightOperand" )
		LowerPriority expr;
		
	}

}