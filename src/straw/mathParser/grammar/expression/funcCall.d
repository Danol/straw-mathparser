﻿module straw.mathParser.grammar.expression.funcCall;

import straw.mathParser.grammar.argumentList;
import straw.mathParser.grammar.toolkit;

final class Symbol_FunctionCallExpression : Symbol {

public:
	enum name = "functionCallExpression";

public:
	@required
	Symbol_Identifier identifier;

	@required
	Symbol_ArgumentList argumentList;

public:
	string debugStr() {
		return identifier.text ~ argumentList.debugStr;
	}
	Value calculate( Environment env ) {
		if( env.currentNesting++ >= env.maxNesting )
			throw new EvaluationException( "maxNestingReached", posData );

		auto result = env.calculateIdentifier( identifier.text, argumentList.calculate( env ), posData );
		env.currentNesting--;

		return result;
	}
	SemanticInfo[] getSemanticInfo( size_t posInFile ) {
		if( identifier.posData.hasInRange( posInFile ) )
			return identifier.getSemanticInfo( posInFile );

		if( argumentList.posData.hasInRange( posInFile ) )
			return argumentList.getSemanticInfo( posInFile );
		
		return null;
	}

}