﻿module straw.mathParser.grammar.argumentList;

import straw.mathParser.grammar.toolkit;

final class Symbol_ArgumentList : Symbol {
	
public:
	enum name = "argumentList";
	
public:
	@required
	Symbol_Terminal!( "leftBracket", "(" ) leftBracketTerminal;
	
	@optional
	Symbol_Space space1;
	
	@optional
	Args arguments;
	
	@optional
	Symbol_Space space2;
	
	@required @errorChecked( "rightBracket" )
	Symbol_Terminal!( "rightBracket", ")" ) rightBracketTerminal;
	
public:
	string debugStr() {
		return "( " ~ ( arguments ? arguments.argument1.debugStr ~ arguments.otherArguments.map!( a => ", " ~ a.expr.debugStr ).array.join : "" ) ~ " )";
	}
	Values calculate( Environment env ) {
		return ( arguments ? [ arguments.argument1.calculate( env ) ] ~ arguments.otherArguments.map!( a => a.expr.calculate( env ) ).array : null );
	}
	SemanticInfo[] getSemanticInfo( size_t posInFile ) {
		if( arguments.posData.hasInRange( posInFile ) )
			return arguments.getSemanticInfo( posInFile );
		
		return null;
	}
	
private:
	final static class Args : Symbol {

	public:
		enum name = "argumentList_arguments1+";

	public:
		@required
		Symbol_Expression argument1;
		
		@optionalRepeated
		OtherArg[] otherArguments;

	public:
		SemanticInfo[] getSemanticInfo( size_t posInFile ) {
			if( argument1.posData.hasInRange( posInFile ) )
				return argument1.getSemanticInfo( posInFile );
			
			foreach( arg; otherArguments ) {
				if( arg.posData.hasInRange( posInFile ) ) 
					return arg.expr.posData.hasInRange( posInFile ) ? arg.expr.getSemanticInfo( posInFile ) : null;
			}
			
			return null;
		}

	}

	final static class OtherArg : Symbol {
		
	public:
		enum name = "argumentList_argument2+";
		
	public:
		@optional
		Symbol_Space space1;
		
		@required
		Symbol_Terminal!( "comma", "," ) commaTerminal;
		
		@optional
		Symbol_Space space2;
		
		@required @errorChecked( "expression" )
		Symbol_Expression expr;
		
	}
	
}