﻿module straw.mathParser.grammar.statement.assign;

import straw.mathParser.grammar.toolkit;

final class Symbol_AssignStatement : Symbol {

public:
	enum name = "assignStatement";

public:
	@required
	Symbol_Identifier identifier;

	@optional
	Symbol_Space space2;

	@required
	Symbol_Terminal!( "=", "=" ) equalsTerminal;

	@optional
	Symbol_Space space3;

	@required
	Symbol_Expression expr;

public:
	string debugStr() {
		return identifier.text ~ " = " ~ expr.debugStr;
	}
	Value calculate( Environment env ) {
		Value val = expr.calculate( env );
		env.defVariable( identifier.text, val, posData );
		return val;
	}
	SemanticInfo[] getSemanticInfo( size_t posInFile ) {
		if( expr.posData.hasInRange( posInFile ) )
			return expr.getSemanticInfo( posInFile );

		return null;
	}

}