﻿module straw.mathParser.exception;

import straw.parser;
import straw.parser.symbol.symbol;

class SemanticException : ParserException {

public:
	this( string str, PositionData posData, string arg = null ) {
		super( str, posData, arg );
	}

}

class EvaluationException : ParserException {
	
public:
	this( string str, PositionData posData, string arg = null ) {
		super( str, posData, arg );
	}
	
}