﻿module straw.mathParser.func.statistic;

import straw.mathParser.func.toolkit;

shared static this() {
	Environment.newSystemFunction( "avg", ( Environment env, PositionData posData, ComplexValue arg1, ComplexValue[] args ) {
		ComplexValue_t sum = arg1.value;
		
		foreach( arg; args ) 
			sum += arg.value;
		
		return complexValue( sum / ( args.length + 1 ) );
	} );
}