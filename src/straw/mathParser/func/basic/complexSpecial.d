﻿module straw.mathParser.func.basic.complexSpecial;

import straw.mathParser.func.toolkit;

shared static this() {
	Environment.newSystemVariable( "i", complexValue( 0, 1 ) );

	// Conjuged number
	Environment.newSystemFunction( "conj", ( Environment env, PositionData posData, ComplexValue a ) {
			return a.value.conj.complexValue;
		} );

	// Argument/phase
	Environment.newSystemFunction( "arg", ( Environment env, PositionData posData, ComplexValue a ) {
			return a.value.arg.realValue;
		} );
}