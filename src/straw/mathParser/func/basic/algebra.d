﻿module straw.mathParser.func.basic.algebra;

import std.complex;
import std.math;
import straw.mathParser.func.toolkit;

private {
	template Template( T : Value ) {

	shared static this() {
		Environment.newSystemFunction( "#op+", ( T a, T b ) {
				return T.ctor( a + b );
			} );
		Environment.newSystemFunction( "#op-", ( T a, T b ) {
				return T.ctor( a - b );
			} );

		Environment.newSystemFunction( "#op*", ( T a, T b ) {
				return T.ctor( a * b );
			} );
		Environment.newSystemFunction( "#op/", ( PositionData posData, T a, T b ) {
				enforce( b.value != 0, new EvaluationException( "divisionByZero", posData ) );

				return T.ctor( a / b );
			} );

		Environment.newSystemFunction( "#opUnary-", ( T a ) {
				return T.ctor( -a );
			} );

		Environment.newSystemFunction( "abs", ( T a ) {
				return realValue( a.value.abs );
			} );
	}

	}

	alias Real = Template!RealValue;
	alias Complex = Template!ComplexValue;
}

/// Optimizations
shared static this() {
	Environment.newSystemFunction( "#op*", ( ComplexValue a, RealValue b ) {
			return complexValue( a * b );
		} );
	Environment.newSystemFunction( "#op*", ( RealValue a, ComplexValue b ) {
			return complexValue( a * b );
		} );

	Environment.newSystemFunction( "#op/", ( Environment env, PositionData posData, ComplexValue a, RealValue b ) {
			enforce( b.value != 0, new EvaluationException( "divisionByZero", posData ) );
			
			return complexValue( a / b );
		} );
}