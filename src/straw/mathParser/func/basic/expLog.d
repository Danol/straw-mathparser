﻿module straw.mathParser.func.basic.expLog;

import straw.mathParser.func.toolkit;

ComplexValue_t logc( ComplexValue_t x ) {
	return complex( x.abs.log, x.arg );
}

/// Power
shared static this() {
	// Wrapper
	Environment.newSystemFunction( "pow", ( Environment env, PositionData posData, AnyNumericalValue a, AnyNumericalValue b ) {
			return env.calculateIdentifier( "#op^", [ a.value, b.value ], posData );
		} );

	Environment.newSystemFunction( "#op^", ( Environment env, PositionData posData, RealValue a, RealValue b ) {
			if( b.value == 0 )
				return RealConstants.one;
			
			if( a.value == 0 )
				return RealConstants.zero;
			
			if( a.value == 1 )
				return RealConstants.one;
			
			if( b.value == 1 )
				return a;

			if( b.value == -1 )
				return realValue( 1 / a );
			
			if( b.value == 2 )
				return realValue( a * a );

			if( b.value == -2 )
				return realValue( 1/( a * a ) );


			// (-3)^x - needs complex result
			if( a.value < 0 )
				return env.calculateIdentifier( "#op^", [ a.castTo!ComplexValue, b ], posData );
			
			return realValue( pow( a.value, b.value ) );
		} );
	Environment.newSystemFunction( "#op^", ( ComplexValue a, RealValue b ) {
			if( b.value == 0 )
				return cast( Value ) RealConstants.one;
			
			if( a.value == 0 )
				return RealConstants.zero;
			
			if( a.value == 1 )
				return RealConstants.one;
			
			if( b.value == 1 )
				return a;
			
			if( b.value == -1 )
				return complexValue( 1 / a );
			
			if( b.value == 2 )
				return complexValue( a * a );

			if( b.value == -2 )
				return complexValue( 1/( a * a ) );
				
			return complexValue( fromPolar(
					pow( a.abs, b.value ),
					a.arg * b
					) );
		} );
	Environment.newSystemFunction( "#op^", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			return null;
		} );
}

/// Square
shared static this() {
	Environment.newSystemFunction( "sqr", ( RealValue a ) {
			return realValue( a * a );
		} );
	Environment.newSystemFunction( "sqr", ( ComplexValue a ) {
			return complexValue( a * a );
		} );
}

/// Square root
shared static this() {
	Environment.newSystemFunction( "sqrt", ( RealValue a ) {
			if( a.value < 0 )
				return complexValue( 0, a.value );
			else
				return realValue( sqrt( a.value ) );
		} );
	Environment.newSystemFunction( "sqrt", ( Environment env, PositionData posData, ComplexValue a ) {
			return env.calculateIdentifier( "#op^", [ a, realValue( 0.5 ) ], posData );
		} );
}

/// Natural logarithm (ln)
shared static this() {
	Environment.newSystemFunction( "ln", ( RealValue a ) {
			return realValue( log( a ) );
		} );
	Environment.newSystemFunction( "ln", ( ComplexValue a ) {
			return complexValue( logc( a ) );
		} );

	Environment.newSystemFunction( "ln", ( Environment env, PositionData posData, AnyNumericalValue a, RealValue b ) {
			return env.calculateIdentifier( "#op/", [
					env.calculateIdentifier( "ln", [ a ], posData ),
					realValue( log( b.value ) )
				], posData );
		} );
	Environment.newSystemFunction( "ln2", ( Environment env, PositionData posData, AnyNumericalValue a, RealValue b ) {
			return env.calculateIdentifier( "#op/", [
					env.calculateIdentifier( "ln", [ a ], posData ),
					RealConstants.ln2
				], posData );
		} );
	Environment.newSystemFunction( "ln10", ( Environment env, PositionData posData, AnyNumericalValue a, RealValue b ) {
			return env.calculateIdentifier( "#op/", [
					env.calculateIdentifier( "ln", [ a ], posData ),
					RealConstants.ln10
				], posData );
		} );
}

/// Decadic logarithm (log)
shared static this() {
	Environment.newSystemFunction( "log", ( RealValue a ) {
			return realValue( log10( a ) );
		} );
	Environment.newSystemFunction( "log", ( ComplexValue a ) {
			return complexValue( a.logc / LN10 );
		} );
}

/// Euler stuff
shared static this() {
	Environment.newSystemVariable( "e", RealConstants.e );

	Environment.newSystemFunction( "exp", ( Environment env, PositionData posData, AnyNumericalValue a ) {
			return env.calculateIdentifier( "#op^", [ RealConstants.e, a.value ], posData );
		} );
}