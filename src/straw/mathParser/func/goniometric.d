﻿module straw.mathParser.func.goniometric;

import straw.mathParser.func.toolkit;

// pi
shared static this() {
	Environment.newSystemVariable( "pi", RealConstants.pi );
}

// sin, cos, tan, cot
shared static this() {
	Environment.newSystemFunction( "sin", ( Environment env, RealValue a ) {
			return env.envToRad( a.value ).sin.realValue;
		} );
	Environment.newSystemFunction( "cos", ( Environment env, RealValue a ) {
			return env.envToRad( a.value ).cos.realValue;
		} );

	Environment.newSystemFunction( "tan", ( Environment env, PositionData posData, RealValue a ) {
			real angle = env.envToRad( a );
			enforce( abs( angle.cos ) > real.epsilon, new EvaluationException( "divisionByZero", posData ) );

			return angle.tan.realValue;
		} );
	Environment.newSystemFunction( "cot", ( Environment env, PositionData posData, RealValue a ) {
			real angle = env.envToRad( a );
			enforce( abs( angle.sin ) > real.epsilon, new EvaluationException( "divisionByZero", posData ) );

			return realValue( 1 / angle.tan );
		} );
}


// asin, acos, atan, acot
shared static this() {
	Environment.newSystemFunction( "asin", ( Environment env, RealValue a ) {
			return env.radToEnv( asin( a.value ) );
		} );
	Environment.newSystemFunction( "acos", ( Environment env, RealValue a ) {
			return env.radToEnv( acos( a.value ) );
		} );
	Environment.newSystemFunction( "atan", ( Environment env, RealValue a ) {
			return env.radToEnv( atan( a.value ) );
		} );
	Environment.newSystemFunction( "acot", ( Environment env, RealValue a ) {
			return env.radToEnv( atan( 1 / a.value ) );
		} );
}