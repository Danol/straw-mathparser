﻿module straw.mathParser.func.set.set;

import straw.mathParser.func.toolkit;

// set
shared static this() {
	Environment.newSystemFunction( "set", ( Environment env, Value[] values ) {
			return values.generalSetValue;
		} );
}