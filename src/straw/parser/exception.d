﻿module straw.parser.exception;

import std.exception;
import straw.parser.parser;

class ParserException : Throwable {
	
public:
	string arg;
	PositionData posData;
	
public:
	this( string err, PositionData posData, string arg = null ) {
		super( err );
		this.arg = arg;
		this.posData = posData;
	}
	
}

class SyntaxException : ParserException {
	
public:
	this( string err, PositionData posData, string arg = null ) {
		super( err, posData, arg );
	}
	
}
