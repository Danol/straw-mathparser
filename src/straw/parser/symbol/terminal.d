﻿module straw.parser.symbol.terminal;

import std.algorithm;
import straw.parser;
import straw.core.utils.typetuple;

final class Symbol_Terminal( string name_, string terminalStr ) : Symbol {

public:
	enum name = name_;

public:
	static typeof( this ) parse( Parser parser ) {		
		for( size_t pos = 0; pos < terminalStr.length; pos ++ ) {
			if( parser.getChar() != terminalStr[ pos ] )
				return null;

			parser.nextChar();
		}

		return new( parser ) typeof( this )();
	}

}

private alias Symbol_TerminalVariants_Helper( string name, string str ) = Symbol_Terminal!( name ~ "_" ~  str, str );

template Symbol_TerminalVariants_Helper2( string name ) {
	alias Symbol_TerminalVariants_Helper2( string str ) = Symbol_TerminalVariants_Helper!( name, str );
}

alias Symbol_TerminalVariants( string name, string[] terminals ) = Symbol_Variants!( name, staticMap!( Symbol_TerminalVariants_Helper2!name, Tuplify!( string, terminals ) ) );