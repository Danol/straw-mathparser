﻿module straw.parser.symbol.characterSequence;

import straw.parser;

class Symbol_CharacterSequence( string name_, string characters_, alias Mixin = Object ) : Symbol {

	static if( !is( Mixin == Object ) )
		mixin Mixin;

public:
	enum name = name_;
	enum characters = characters_;

public:
	static typeof( this ) parse( Parser parser ) {
		bool found = false;
		
		while( true ) {
			dchar ch = parser.getChar();
			
			bool isInSequence = false;
			foreach( testedCh; characters ) {
				if( ch == testedCh ) {
					isInSequence = true;
					found = true;
					break;
				}
			}

			if( !isInSequence )
				return found ? new( parser ) typeof( this )() : null;

			parser.nextChar();
		}
	}
	
}
