module appMain;
version( APP ):

import core.memory;
import std.algorithm;
import std.array;
import std.conv;
import std.datetime;
import std.exception;
import std.stdio;
import straw.mathParser.env;
import straw.mathParser.grammar.entry;
import straw.mathParser.value;
import straw.parser;

__gshared Parser parser;

string parseThis( string text, Environment env, bool write = true ) {
	auto result = appender!( char[] )();

	if( write )
		writeln( ">> ", text );

	Symbol_Entry parsed;
	parser.reuse( text );

	StopWatch sw, sw2;
	sw.start();
	try {
		parsed = parser.parse!Symbol_Entry();

		enforce( parsed, new SyntaxException( "unexpectedSymbol", PositionData( 0, 0, 0, parser.sourceLen ) ) );
		enforce( parser.getChar == 0, new SyntaxException( "unexpectedSymbol", PositionData( parser.posData, parser.sourceLen - parser.pos ) ) );
	}
	catch( ParserException e )
		writeln( "EXCEPTION: ", e.msg, ": ", e.arg );

	sw.stop();


	//visualiseTree( parsed );

	if( parsed ) {
		writeln( "  ", parsed.debugStr );

		sw2.start();

		Values vals;
		try
			vals = parsed.calculate( env );

		catch( ParserException e )
			writeln( "CALC EXCEPTION: ", e.msg, ": ", e.arg );

		sw2.stop();

		if( vals.length ) {
			vals[ 0 ].toString( env, result );

			foreach( val; vals[ 1 .. $ ] ) {
				result ~= ", ";
				val.toString( env, result );
			}
		}

		writeln( "  RESULT: ", result.data );

		parsed = null;

	} else
		writeln( "  ERROR" );

	writeln( "  PARSING TOOK ", sw.peek.usecs, "us" );
	writeln( "  CALCULATIONS TOOK ", sw2.peek.usecs, "us" );
	writeln( "  TOOK ", sw.peek.usecs + sw2.peek.usecs, "us" );

	sw.reset();

	return cast( string ) result.data;
}

string enumTest() {
	Environment env = new Environment();
	auto parsed = Parser.staticParse!Symbol_Entry( text );
	auto vals = parsed.calculate( env );
	auto result = vals.map!( v => v ? v.to!string : "ERR" ).array.join( "; " );
	return result;
}

void main() {

	/*enum r = enumTest;
	writeln( r, "TEST" );
	writeln();
	writeln();*/

	Environment env = new Environment();

	version( unittest ) {
		foreach( a; 0 .. tests.length / 2 ) {
			assert( parseThis( tests[ a * 2 ], env, false ) == tests[ a*2+1 ] );
		}
	}

	/*parseThis( "5 - 2 - 3", env );
	parseThis( "x = 5; y = 6", env );
	parseThis( "5" ~ "+5".replicate( 1000 ), env );*/

	parser = new Parser();

	while( true ) {
		write( ">> " );
		string str = readln();

		if( str.length < 2 )
			break;

		parseThis( str, env, false );
	}
}

version( unittest ) {
	enum tests = [
		"5+3", "8",
		"i*i", "-1",
		"i^2", "-1",
	];
}