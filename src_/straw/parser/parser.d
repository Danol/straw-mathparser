﻿module straw.parser.parser;

import core.bitop;
import core.memory;
import std.exception;
import std.traits;
import std.utf;
import straw.parser;

struct PositionData {
	
public:
	size_t line, posOnLine, posInFile, length;

public:
	this( size_t line, size_t posOnLine, size_t posInFile, size_t length = 0 ) {
		this.line = line;
		this.posOnLine = posOnLine;
		this.posInFile = posInFile;
		this.length = length;
	}
	this( PositionData data, size_t length ) {
		this = data;
		this.length = length;
	}
	
}

final class Parser {
	
public:
	final class Bookmark {

	private:
		PositionData posd_;
		size_t pos_, objectStackChunk_, objectStackPosInChunk_;
		
	public:
		@property PositionData posData() {
			return posd_;
		}
		@property size_t pos() {
			return pos_;
		}

	public:
		this() {
			posd_ = this.outer.posd_;
			pos_ = this.outer.pos_;

			objectStackChunk_ = this.outer.objectStackChunk_;
			objectStackPosInChunk_ = this.outer.objectStackPosInChunk_;
		}

	public:
		/// Rewinds the sourceScanner to the state (position) when bookmark was created
		void rewind() {
			this.outer.posd_ = this.posd_;
			this.outer.pos_ = this.pos_;

			this.outer.objectStackChunk_ = objectStackChunk_;
			this.outer.objectStackPosInChunk_ = objectStackPosInChunk_;
		}
		
	}

private:
	PositionData posd_ = PositionData( 1, 0, 0 );
	size_t pos_, objectStackChunk_, objectStackPosInChunk_;
	string source;

private:
	enum objectStackChunking = 1024 * 1024;
	void*[] objectStack;
	
public:
	@property PositionData posData() {
		return posd_;
	}
	@property size_t pos() {
		return pos_;
	}
	@property size_t sourceLen() {
		return source.length;
	}

public:
	this( string source ) {
		this.source = source ~ 0;

		objectStack ~= GC.malloc( objectStackChunking );
	}
	this() {
		objectStack ~= GC.malloc( objectStackChunking );
	}

	void reuse( string source ) {
		pos_ = pos_.init;
		posd_ = posd_.init;

		objectStackChunk_ = objectStackChunk_.init;
		objectStackPosInChunk_ = objectStackChunk_.init;

		this.source = source ~ 0;
	}

public:
	/// Gets one character from the source
	dchar getChar() {
		// TODO Bounds check
		auto tmpPos = pos_;
		return source.decode( tmpPos );
	}

	/// Moves the scanner one char forward
	void nextChar() {
		// TODO Bounds check
		dchar ch = source.decode( pos_ );

		if( ch == '\n' ) {
			posd_.line ++;
			posd_.posOnLine = 0;
		}

		posd_.posInFile ++;
		posd_.posOnLine ++;
	}

	/// Returns part of the source
	string getSourcePart( size_t start, size_t end ) {
		// TODO Bounds check
		return source[ start .. end ];
	}

public:
	/// Creates a bookmark at the current position
	Bookmark createBookmark() {
		return new Bookmark();
	}

public:
	static T staticParse( T )( string str )
		if( is( T : Symbol ) )
	{
		Parser parser = new Parser( str );
		T result = parser.parse!T;
		
		enforce( parser.getChar == 0, new SyntaxException( "unexpectedSymbol", PositionData( parser.posData, parser.sourceLen - parser.pos ) ) );
		return result;
	}
	
	/// Attempts to parse a symbol on the current position in the parser, returns null if fails
	T parse( T )()
		if( is( T : Symbol ) )
	{
		//	static assert( isFinalClass!T, "Class '" ~ T.stringof ~ "' must be final" );
		static assert( __traits( compiles, T.name ), "Class '" ~ T.stringof ~ "' must have a T.name member" );

		Bookmark bookmark = createBookmark();
		T result;

		// Symbol has custom parse function
		static if( __traits( compiles, T.parse( this ) ) ) {
			result = T.parse( this );
		}
		
		// Conventional parsing
		else {
			result = new( this ) T();
			bool success = true;
			size_t posInFileuiredSymbols;
			
			foreach( memName; __traits( allMembers, T ) ) {
				if( success ) {
					static if( __traits( compiles, typeof( __traits( getMember, result, memName ) ) ) ) {
						alias MemType = typeof( __traits( getMember, result, memName ) );
						
						foreach( attr; __traits( getAttributes, __traits( getMember, T, memName ) ) ) {
							// Required symbol - fail if not found
							static if( is( attr == Symbol.required ) ) {
								static assert( is( MemType : Symbol ), "Member '" ~ memName ~ "' marked with @required must inherit from Symbol" );
								posInFileuiredSymbols ++;
								
								MemType member = parse!MemType();
								if( member )
									__traits( getMember, result, memName ) = member;
								else {
									success = false;
									
									checkErrorChecked!( T, memName )( bookmark );
								}
								
								break;
							}
							
							// Optional symbol - nothing happens if not found
							else static if( is( attr == Symbol.optional ) ) {
								static assert( is( MemType : Symbol ), "Member '" ~ memName ~ "' marked with @optional must inherit from Symbol" );
								
								__traits( getMember, result, memName ) = parse!MemType();
								break;
							}

							// Errorneous symbol - it must not be there
							else static if( is( typeof( attr ) == Symbol.AttrErrorneous ) ) {
								static assert( is( MemType : Symbol ), "Member '" ~ memName ~ "' marked with @optional must inherit from Symbol" );
								
								MemType member = parse!MemType();
								enforce( !member, new SyntaxException( attr.errName, member.posData ) );

								break;
							}
							
							// Required repeated - must be at least once but can be up to nx
							else static if( is( attr == Symbol.posInFileuiredRepeated ) || is( attr == Symbol.optionalRepeated ) ) {
								static assert( isDynamicArray!MemType && is( ForeachType!MemType : Symbol ), "Member '" ~ memName ~ "' marked with @posInFileuiredRepeated or @optionalRepeated must be an array with value type that inherits from Symbol" );
								alias BaseMemType = ForeachType!MemType;
								
								size_t cnt;
								while( true ) {
									BaseMemType member = parse!BaseMemType();
									
									if( !member )
										break;
									
									cnt ++;
									__traits( getMember, result, memName ) ~= member;
								}
								
								if( is( attr == Symbol.posInFileuiredRepeated ) && !cnt ) {
									success = false;
									
									checkErrorChecked!( T, memName )( bookmark );
								}
								
								posInFileuiredSymbols ++;
								break;
							}
						}
					}
				}
			}
			
			assert( posInFileuiredSymbols > 0, "Class '" ~ T.name ~ "' has no required symbols" );
			
			if( !success ) {
				//delete result;
				result = null;
			}
		}
		
		if( result )
			result.setData( bookmark.posData, getSourcePart( bookmark.pos, pos ) );
		
		else
			bookmark.rewind();
		
		return result;
	}

public:
	void* getSpaceForObject( size_t space ) {
		enforce( space != 0 );
		enforce( space < objectStackChunking );

		if( objectStackPosInChunk_ + space >= objectStackChunking ) {
			objectStackChunk_ ++;
			objectStackPosInChunk_ = 0;

			enforce( objectStackChunk_ <= objectStack.length );
			if( objectStackChunk_ == objectStack.length )
				objectStack ~= GC.malloc( objectStackChunking );
		}

		objectStackPosInChunk_ += space;
		return objectStack[ objectStackChunk_ ] + objectStackPosInChunk_ - space;
	}

private:
	void checkErrorChecked( T, string memName )( ref Bookmark bookmark ) {
		foreach( attr; __traits( getAttributes, __traits( getMember, T, memName ) ) ) {
			static if( is( typeof( attr ) == Symbol.AttrErrorChecked ) )
				throw new SyntaxException( "expectedSymbol", PositionData( posData, sourceLen - pos ), "@" ~ attr.errName );
		}
	}

}