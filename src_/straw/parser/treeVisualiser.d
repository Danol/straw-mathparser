﻿module straw.parser.treeVisualiser;

import std.traits;
import std.stdio;
import straw.parser.parser;
import straw.parser.symbol.symbol;

enum visualiseTree_offsetStr = "  ";

/// Prints the symbol tree to the stdout
void visualiseTree( T )( T symbol )
	if( is( T : Symbol ) )
{
	writeln( "ROOT [", T.name, "]" );
	visualiseTreeImpl( symbol, visualiseTree_offsetStr );
}

void visualiseTreeImpl( T )( T symbol, string offset )
	if( is( T : Symbol ) )
{
	if( !symbol ) {
		writeln( offset, "NULL" );
		return;
	}

	// Symbol has custom parse function
	static if( __traits( compiles, T.parse ) ) {	
		static if( __traits( compiles, symbol.visualise( offset ) ) )
			symbol.visualise( offset );

		else
			writeln( offset, "CUSTOM '", symbol.text, "'" );
	}
	
	// Conventional parsing
	else {
		// Doesn't work with parenting
		// static assert( !__traits( compiles, T.alternatives ), "Class '" ~ T.stringof ~ "' must not have T.alternatives when being final" );

		foreach( memName; __traits( allMembers, T ) ) {
			static if( __traits( compiles, typeof( __traits( getMember, symbol, memName ) ) ) ) {
				alias MemType = typeof( __traits( getMember, symbol, memName ) );
				
				foreach( attr; __traits( getAttributes, __traits( getMember, T, memName ) ) ) {
					// Required symbol
					static if( is( attr == Symbol.posInFileuired ) ) {
						writeln( offset, "REQUIRED [", MemType.name, "] ", memName );
						visualiseTreeImpl( __traits( getMember, symbol, memName ), offset ~ visualiseTree_offsetStr );
						break;
					}
					
					// Optional symbol
					else static if( is( attr == Symbol.optional ) ) {
						writeln( offset, "OPTIONAL [", MemType.name, "] ", memName );
						visualiseTreeImpl( __traits( getMember, symbol, memName ), offset ~ visualiseTree_offsetStr );
						break;
					}

					// Required repeated symbol
					else static if( is( attr == Symbol.posInFileuiredRepeated ) ) {
						foreach( mem; __traits( getMember, symbol, memName ) ) {
							writeln( offset, "REQUIRED_REPEATED [", ForeachType!MemType.name, "] ", memName );
							visualiseTreeImpl( mem, offset ~ visualiseTree_offsetStr );
						}
						break;
					}

					// Optional repeated symbol
					else static if( is( attr == Symbol.optionalRepeated ) ) {
						foreach( mem; __traits( getMember, symbol, memName ) ) {
							writeln( offset, "OPTIONAL_REPEATED [", ForeachType!MemType.name, "] ", memName );
							visualiseTreeImpl( mem, offset ~ visualiseTree_offsetStr );
						}
						break;
					}
				}
			}
		}
	}
}