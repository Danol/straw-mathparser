﻿module straw.parser.symbol.variants;

import std.exception;
import std.stdio;
import std.typetuple;
import straw.parser;

private template ExpandedVariants( Variants ... ) {
	static if( Variants.length == 0 )
		alias ExpandedVariants = TypeTuple!();

	else static if( __traits( compiles, Variants[ 0 ].IS_Symbol_Variants ) )
		alias ExpandedVariants = TypeTuple!( Variants[ 0 ].Variants, ExpandedVariants!( Variants[ 1 .. $ ] ) );

	else
		alias ExpandedVariants = TypeTuple!( Variants[ 0 ], ExpandedVariants!( Variants[ 1 .. $ ] ) );
}

final class Symbol_Variants( string name_, Variants_ ... ) : Symbol {

private:
	alias This = typeof( this );

public:
	enum name = name_;
	enum IS_Symbol_Variants = true;
	alias Variants = ExpandedVariants!Variants_;

public:
	size_t variantId;
	Symbol symbol;

public:
	this( Symbol symbol, size_t variantId ) {
		this.symbol = symbol;
		this.variantId = variantId;
	}

public:
	static This parse( Parser parser ) {
		foreach( a, Var; Variants ) {
			if( Var result = parser.parse!Var() )
				return new( parser ) This( result, a );
		}

		return null;
	}
	void visualise( string offset ) {
		foreach( Var; Variants ) {
			if( Var v = cast( Var ) symbol ) {
				writeln( offset, "VARIANT [", Var.name, "]" );
				visualiseTreeImpl( v, offset ~ visualiseTree_offsetStr );
				return;
			}
		}

		writeln( offset, "VARIANT FAIL" );
	}


public:
	auto ref opDispatch( string str, Args ... )( auto ref Args args ) {
		enforce( symbol );

		foreach( Var; Variants ) {
			if( Var var = cast( Var ) symbol ) {
				static if( Args.length == 0 )
					return __traits( getMember, var, str );

				else static if( Args.length == 1 )
					return __traits( getMember, var, str ) = args[ 0 ];

				else
					return __traits( getMember, var, str )( args );
			}
		}

		assert( 0 );
	}

}