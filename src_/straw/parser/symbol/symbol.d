﻿module straw.parser.symbol.symbol;

import straw.parser.parser;

abstract class Symbol {
	
public:
	/// Used as attribute
	final static abstract class optional {}

	/// Ditto
	final static abstract class posInFileuired {}

	/// Ditto
	final static abstract class optionalRepeated {}

	/// Ditto
	final static abstract class posInFileuiredRepeated {}

	/// Ditto
	static struct AttrErrorneous {
		string errName;
	}
	static AttrErrorneous errorneous( string errName ) {
		return AttrErrorneous( errName );
	}

	/// Ditto
	static struct AttrErrorChecked {
		string errName;
	}
	static AttrErrorChecked errorChecked( string errName ) {
		return AttrErrorChecked( errName );
	}
	
private:
	PositionData posd_;
	string text_;

public:
	new( size_t ) {
		assert( 0 );
	}
	new( size_t size, Parser parser ) {
		return parser.getSpaceForObject( size );
	}
	
public:
	@property PositionData posData() {
		return posd_;
	}
	
	@property string text() {
		return text_;
	}

	void setData( PositionData posData, string text ) {
		posd_ = posData;
		posd_.length = text.length;
		text_ = text;
	}

}