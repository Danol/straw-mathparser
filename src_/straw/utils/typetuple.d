﻿module straw.utils.typetuple;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.typetuple;

final abstract class EnclosedTypeTuple( Args ... ) {
	alias tuple = Args;
}

template Tuplify( T, T[] array_ ) {
	enum Tuplify = mixin( "TypeTuple!( " ~ iota( array_.length ).map!( a => "array_[ " ~ a.to!string ~ " ]" ).joiner( ", " ).array ~ " )" );
}

template Detuplify( Arr... ) {
	enum Detuplify = mixin( "[ " ~ iota( Arr.length ).map!( a => "Arr[ " ~ a.to!string ~ " ]" ).joiner( ", " ).array ~ " ]" );
}