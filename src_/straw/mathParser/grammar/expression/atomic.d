﻿module straw.mathParser.grammar.expression.atomic;

import straw.mathParser.grammar.argumentList;
import straw.mathParser.grammar.expression.block;
import straw.mathParser.grammar.expression.funcCall;
import straw.mathParser.grammar.expression.set;
import straw.mathParser.grammar.toolkit;

class Symbol_Atomic : Symbol {

public:
	enum name = "atomic";

public:
	@required
	Base base;

	@optionalRepeated
	ChainMember[] chain;

public:
	Value calculate( Environment env ) {
		Value result = base.calculate( env );

		foreach( mem; chain ) {
			if( env.currentNesting++ >= env.maxNesting )
				throw new EvaluationException( "maxNestingReached", posData );

			result = env.calculateIdentifier( mem.identifier.text, [ result ] ~ ( mem.argumentList ? mem.argumentList.calculate( env ) : null ), mem.posData );

			env.currentNesting --;
		}

		return result;
	}
	string debugStr() {
		string result = base.debugStr();

		foreach( mem; chain )
			result ~= "." ~ mem.identifier.text ~ ( mem.argumentList ? mem.argumentList.debugStr : null );

		return result;
	}

public:
	alias Base = Symbol_Variants!(
		"atomic_base",

		Symbol_Block,
		Symbol_Set,
		Symbol_FunctionCallExpression,
		Symbol_Identifier,
		Symbol_Number
		
		);
	static class ChainMember : Symbol {

	public:
		enum name = "atomic_chainMember";

	public:
		@required
		Symbol_Terminal!( "dot", "." ) dot;

		@required
		Symbol_Identifier identifier;

		//@required @errorChecked( "argumentList" )
		@optional
		Symbol_ArgumentList argumentList;

	}

}