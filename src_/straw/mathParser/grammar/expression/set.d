﻿module straw.mathParser.grammar.expression.set;

import straw.mathParser.grammar.toolkit;

final class Symbol_Set : Symbol {

public:
	enum name = "set";
	
public:
	@required
	Symbol_Terminal!( "leftCurlyBracket", "{" ) leftBracketTerminal;
	
	@optional
	Symbol_Space space1;
	
	@optional
	Args arguments;
	
	@optional
	Symbol_Space space2;
	
	@required @errorChecked( "rightCurlyBracket" )
	Symbol_Terminal!( "rightCurlyBracket", "}" ) rightBracketTerminal;
	
public:
	string debugStr() {
		return "{ " ~ ( arguments ? arguments.argument1.debugStr ~ arguments.otherArguments.map!( a => ", " ~ a.expr.debugStr ).array.join : "" ) ~ " }";
	}
	Value calculate( Environment env ) {
		return generalSetValue( arguments ? arguments.calculate( env ) : null );
	}
	
private:
	final static class Args : Symbol {
		
	public:
		enum name = "set_arguments1+";
		
	public:
		@required
		Symbol_Expression argument1;
		
		@optionalRepeated
		OtherArg[] otherArguments;

		Values calculate( Environment env ) {
			return [ argument1.calculate( env ) ] ~ otherArguments.map!( a => a.expr.calculate( env ) ).array;
		}
		
	}
	
	final static class OtherArg : Symbol {
		
	public:
		enum name = "set_arguments2+";
		
	public:
		@optional
		Symbol_Space space1;
		
		@required
		Symbol_Terminal!( "comma", "," ) commaTerminal;
		
		@optional
		Symbol_Space space2;
		
		@required @errorChecked( "expression" )
		Symbol_Expression expr;
		
	}
	
}