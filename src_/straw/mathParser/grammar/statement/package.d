﻿module straw.mathParser.grammar.statement;
public {
	import straw.mathParser.grammar.statement.assign;
}

import straw.mathParser.grammar.toolkit;
alias Symbol_AnyStatement = Symbol_Variants!(
	"anyStatement",

	Symbol_AssignStatement
	);