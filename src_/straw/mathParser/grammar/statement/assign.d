﻿module straw.mathParser.grammar.statement.assign;

import straw.mathParser.grammar.toolkit;

final class Symbol_AssignStatement : Symbol {

public:
	enum name = "assignStatement";

public:
	@required
	Symbol_Identifier identifier;

	@optional
	Symbol_Space space2;

	@required
	Symbol_Terminal!( "=", "=" ) equalsTerminal;

	@optional
	Symbol_Space space3;

	@required
	Symbol_Expression expr;

public:
	string debugStr() {
		return identifier.text ~ " = " ~ expr.debugStr;
	}
	Value calculate( Environment env ) {
		env.defVariable( identifier.text, expr.calculate( env ), posData );
		return null;
	}

}