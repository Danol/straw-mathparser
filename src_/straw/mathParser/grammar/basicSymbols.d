﻿module straw.mathParser.grammar.basicSymbols;

import straw.mathParser.grammar.toolkit;

alias Symbol_Space = Symbol_CharacterSequence!( "space", " \r" ~ 10 ~ 13 );
alias Symbol_SpaceNoNewline = Symbol_CharacterSequence!( "space", " \t" );

alias Symbol_LetterSequence = Symbol_CharacterSequence!( "characterSequence", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" );
alias Symbol_LetterAndDigitSequence = Symbol_CharacterSequence!( "characterSequence", Symbol_LetterSequence.characters ~ Symbol_DigitSequence.characters );

alias Symbol_DigitSequence = Symbol_CharacterSequence!( "digitSequence", "0123456789" );
alias Symbol_HexDigitSequence = Symbol_CharacterSequence!( "hexDigitSequence", Symbol_DigitSequence.characters ~ "abcdefABCDEF" );

final class Symbol_Identifier : Symbol {

public:
	enum name = "identifier";

public:
	@required
	Symbol_LetterSequence part1;

	@optional
	Symbol_LetterAndDigitSequence part2;

public:
	string debugStr() {
		return this.text;
	}
	/// Calculate is called only when the identifier is used as a variable. Otherwise, calculate in Symbol_FunctionCall or so is called
	Value calculate( Environment env ) {
		return env.calculateIdentifier( this.text, null, posData );
	}

}

/// Sucks up everthing till the end of the document = used for checking for leftovers
final class Symbol_Anything : Symbol {
	
public:
	enum name = "anything";
	
public:
	static typeof( this ) parse( Parser parser ) {		
		if( parser.getChar() == 0 )
			return null;

		while( parser.getChar() != 0 )
			parser.nextChar();		

		return new( parser ) typeof( this )();
	}
	
}