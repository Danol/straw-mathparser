﻿module straw.mathParser.grammar.argumentList;

import straw.mathParser.grammar.toolkit;

final class Symbol_ArgumentList : Symbol {
	
public:
	enum name = "argumentList";
	
public:
	@required
	Symbol_Terminal!( "leftBracket", "(" ) leftBracketTerminal;
	
	@optional
	Symbol_Space space1;
	
	@optional
	Args arguments;
	
	@optional
	Symbol_Space space2;
	
	@required @errorChecked( "rightBracket" )
	Symbol_Terminal!( "rightBracket", ")" ) rightBracketTerminal;
	
public:
	string debugStr() {
		return "( " ~ ( arguments ? arguments.argument1.debugStr ~ arguments.otherArguments.map!( a => ", " ~ a.expr.debugStr ).array.join : "" ) ~ " )";
	}
	Values calculate( Environment env ) {
		return ( arguments ? [ arguments.argument1.calculate( env ) ] ~ arguments.otherArguments.map!( a => a.expr.calculate( env ) ).array : null );
	}
	
private:
	final static class Args : Symbol {

	public:
		enum name = "argumentList_arguments1+";

	public:
		@required
		Symbol_Expression argument1;
		
		@optionalRepeated
		OtherArg[] otherArguments;

	}

	final static class OtherArg : Symbol {
		
	public:
		enum name = "argumentList_argument2+";
		
	public:
		@optional
		Symbol_Space space1;
		
		@required
		Symbol_Terminal!( "comma", "," ) commaTerminal;
		
		@optional
		Symbol_Space space2;
		
		@required @errorChecked( "expression" )
		Symbol_Expression expr;
		
	}
	
}