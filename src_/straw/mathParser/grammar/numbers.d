﻿module straw.mathParser.grammar.numbers;

import std.conv;
import std.math;
import straw.mathParser.grammar.toolkit;

alias Symbol_Number = Symbol_Variants!(
	"number",

	Symbol_Number_Decimal,
	Symbol_Number_Nondecimal
	);

final class Symbol_Number_Nondecimal : Symbol {

public:
	enum name = "nondecimalNumber";

public:
	@optional
	Symbol_Terminal!( "minus", "-" ) minus;

	@required
	Symbol_DigitSequence digits;

	@optional
	Symbol_ScientificExponent exp;

public:
	string debugStr() {
		return text;
	}
	Value calculate( Environment env ) {
		return realValue( text.to!real );
	}

}

final class Symbol_Number_Decimal : Symbol {

public:
	enum name = "decimalNumber";

public:
	@optional
	Symbol_Terminal!( "minus", "-" ) minus;

	@optional
	Symbol_DigitSequence integralPart;

	@required
	Symbol_Terminal!( "decimalMark", "." ) decimalMark;

	@required
	Symbol_DigitSequence decimalPart;

	@optional
	Symbol_ScientificExponent exp;

public:
	string debugStr() {
		return text;
	}
	Value calculate( Environment env ) {
		return realValue( text.to!real );
	}
	
}

final class Symbol_ScientificExponent : Symbol {
	
public:
	enum name = "scientificExponent";
	
public:
	@required
	Symbol_Terminal!( "e", "e" ) exp;
	
	@optional
	Symbol_Terminal!( "minus", "-" ) minus;
	
	@required
	Symbol_DigitSequence expNumber;
	
}