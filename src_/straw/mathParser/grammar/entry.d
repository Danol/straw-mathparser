﻿module straw.mathParser.grammar.entry;

import straw.mathParser.grammar.toolkit;
import straw.mathParser.grammar.statement;

/// Set of expression and statements separated with semicolon
final class Symbol_Entry : Symbol {
	
public:
	enum name = "expression";
	alias TopExpr = Symbol_Variants!(
		"exprOrStatement",
		
		Symbol_AnyStatement,
		Symbol_Expression
		);
	
public:
	@optional
	Symbol_Space space1;

	@required
	TopExpr left;
	
	@optionalRepeated
	RightExpr[] rights;

	@optional
	Symbol_Space space2;

	@optional
	Symbol_Terminal!( "semicolon", ";" ) semicolonTerminal;

	@optional
	Symbol_Space space3;
	
public:
	string debugStr() {
		return ( rights ? "( " : "" ) ~ left.debugStr ~ rights.map!( r => "; " ~ r.expr.debugStr ).array.join ~ ( rights ? " ) " : "" );
	}
	Values calculate( Environment env ) {
		env.currentNesting = 0;

		Values result = [ left.calculate( env ) ];

		foreach( right; rights )
			result ~= right.expr.calculate( env );

		return result.filter!( a => a !is null ).array;
	}
	
private:
	static final class RightExpr : Symbol {
		
	public:
		enum name = "expression_right";
		
	public:
		@optional
		Symbol_Space space1;
		
		@required
		Symbol_Terminal!( "semicolon", ";" ) semicolonTerminal;

		@optional
		Symbol_Space space2;
		
		@required
		TopExpr expr;
		
	}
	
}