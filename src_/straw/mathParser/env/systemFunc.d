﻿module straw.mathParser.env.systemFunc;

import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.traits;
import std.typetuple;
import straw.mathParser.env.env;
import straw.mathParser.env.func;
import straw.mathParser.value;
import straw.parser.parser;

final class SystemFunction( Args ... ) : Function {
	
public:
	alias IFunc = Value function( Args );
	enum isSystemArg( T ) = is( T == Environment ) || is( T == PositionData );

public:
	static if( Args.length )
		enum isVariadic = isDynamicArray!( Args[ $ - 1 ] );
	else
		enum isVariadic = false;

	static if( isVariadic ) {
		alias NonVariadicArgs = Args[ 0 .. $ - 1 ];
		alias VariadicArg = ForeachType!( Args[ $ - 1 ] );
	}
	else {
		alias NonVariadicArgs = Args;
	}
	
private:
	immutable IFunc ifunc;
	
public:
	this( IFunc ifunc, string file, int line ) {
		super( mixin( {
			string[] result;
			foreach( a, Arg; Args ) {
				static if( isSystemArg!Arg ) {
					// Do nothing
				}
				else static if( isDynamicArray!Arg ) {
					static assert( a == Args.length - 1 );
					result ~= "ForeachType!( Args[" ~ a.to!string ~ " ] ).sname";
				}
				else {
					result ~= "Args[" ~ a.to!string ~ " ].sname";
				}
			}
			return "[ " ~ result.join( ", " ) ~ " ]";
		}() ), file, line );
		this.ifunc = ifunc;
	}

public:
	override Casts castsRequired( Values vals ) {
		Casts result;
		size_t a = 0;

		foreach( Arg; NonVariadicArgs ) {
			static if( isSystemArg!Arg )
			{}

			else if( a >= vals.length )
				return errorCasts;

			else if( cast( Arg ) vals[ a ] )
				result[ 1 ] ++, a ++;

			else if( vals[ a ].castableTo.canFind( Arg.sname ) )
				result[ Arg.slevel - vals[ a ].level + 1 ] ++, a ++;

			else
				return errorCasts;
		}

		static if( isVariadic ) {
			auto a_ = a;
			foreach( val; vals[ a_ .. $ ] ) {
				if( cast( VariadicArg ) val )
					result[ 1 ] ++, a ++;

				else if( val.castableTo.canFind( VariadicArg.sname ) )
					result[ VariadicArg.slevel - val.level + 1 ] ++, a ++;
				
				else
					return errorCasts;
			}
		}

		if( a != vals.length )
			return errorCasts;

		return result;
	}

	override Value call( Environment env, ref PositionData posData, Values args ) {
		mixin( {
			string[] result;
			size_t a = 0;
			foreach( arga, Arg; NonVariadicArgs ) {
				static if( is( Arg == PositionData ) ) {
					result ~= "posData";
				}
				else static if( is( Arg == Environment ) ) {
					result ~= "env";
				}	
				else {
					result ~= "cast( Args[" ~ arga.to!string ~ "] ) args[" ~ a.to!string ~ "].castTo( Args[" ~ arga.to!string ~ "].sname )";
					a ++;
				}
			}
			static if( isVariadic )
				result ~= "args[ " ~ a.to!string ~ " .. $ ].map!( r => cast( VariadicArg ) r.castTo( VariadicArg.sname ) ).array";

			return "return ifunc( " ~ result.join( ", " ) ~ " );";
		}() );
	}

}
