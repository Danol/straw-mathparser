﻿module straw.mathParser.value.any;

import std.conv;
import straw.mathParser.value;
import straw.mathParser.env;

alias AnyNumericalValue = AnyValue!( "anyNumerical", 2 );

final class AnyValue( string sname_, ushort slevel_ ) : Value {
	
public:
	enum IS_ANY_VALUE_TYPE = true;

	enum sname = sname_;
	enum slevel = slevel_;
	enum scastableTo = [];
	
public:
	Value value;
	
public:
	this( Value value ) {
		this.value = value;
	}
	
public:
	override @property string name() {
		return sname;
	}
	override @property ushort level() {
		return slevel;
	}
	override @property string[] castableTo() {
		return scastableTo;
	}
	
public:
	override Value castToImpl( string targetName ) {
		assert( 0 );
	}
	
public:
	override void toString( Environment env, ref Sink sink ) {
		value.toString( env, sink );
	}
	
}