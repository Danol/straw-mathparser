﻿module straw.mathParser.value.complex;

import std.algorithm;
import std.complex;
import std.conv;
import std.math;
import straw.mathParser.value;
import straw.mathParser.env;

Value complexValue( ComplexValue_t value ) {
	if( value.im == 0 )
		return realValue( value.re );
		
	return new ComplexValue( value );
}
Value complexValue( real re, real im ) {
	if( im == 0 )
		return realValue( re );
	
	return new ComplexValue( complex( re, im ) );
}

alias ComplexValue_t = Complex!real;

final class ComplexValue : Value {
	mixin Value_mix!( "complex", 1, AnyNumericalValue );

public:
	ComplexValue_t value;
	alias value this;
	
public:
	this( ComplexValue_t value ) {
		this.value = value;
	}

public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {
			
			case AnyNumericalValue.sname:
				return new AnyNumericalValue( this );
				
			default:
				assert( 0 );
				
		}
	}

public:
	override void toString( Environment env, ref Sink sink ) {
		if( value.re == 0 )
			value.im.toString( env, sink ), sink ~= " i";
		else
			value.re.toString( env, sink ), sink ~= " + ", value.im.toString( env, sink ), sink ~= " i";
	}
	
}