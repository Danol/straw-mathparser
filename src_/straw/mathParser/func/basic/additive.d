﻿module straw.mathParser.func.basic.additive;

import straw.mathParser.func.toolkit;

/// Plus
shared static this() {
	Environment.newSystemFunction( "#op+", ( RealValue a, RealValue b ) {
			return realValue( a + b );
		} );
	Environment.newSystemFunction( "#op+", ( ComplexValue a, ComplexValue b ) {
			return complexValue( a + b );
		} );
}

/// Minus
shared static this() {
	Environment.newSystemFunction( "#op-", ( RealValue a, RealValue b ) {
			return realValue( a - b );
		} );
	Environment.newSystemFunction( "#op-", ( ComplexValue a, ComplexValue b ) {
			return complexValue( a - b );
		} );
}

/// Unary minus
shared static this() {
	Environment.newSystemFunction( "#opUnary-", ( RealValue a ) {
			return realValue( -a );
		} );
	Environment.newSystemFunction( "#opUnary-", ( ComplexValue a ) {
			return complexValue( -a );
		} );

}