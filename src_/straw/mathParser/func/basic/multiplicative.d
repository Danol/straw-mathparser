﻿module straw.mathParser.func.basic.multiplicative;

import straw.mathParser.func.toolkit;

/// Multiply
shared static this() {
	Environment.newSystemFunction( "#op*", ( RealValue a, RealValue b ) {
			return realValue( a * b );
		} );
	Environment.newSystemFunction( "#op*", ( ComplexValue a, ComplexValue b ) {
			return complexValue( a * b );
		} );

	// Complex values optimization
	Environment.newSystemFunction( "#op*", ( ComplexValue a, RealValue b ) {
			return complexValue( a * b );
		} );
	Environment.newSystemFunction( "#op*", ( RealValue a, ComplexValue b ) {
			return complexValue( a * b );
		} );
}

/// Divide
shared static this() {
	Environment.newSystemFunction( "#op/", ( PositionData posData, RealValue a, RealValue b ) {
			enforce( b.value != 0, new EvaluationException( "divisionByZero", posData ) );

			return realValue( a / b );
		} );
	Environment.newSystemFunction( "#op/", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			enforce( b.value.abs != 0, new EvaluationException( "divisionByZero", posData ) );

			return complexValue( a / b );
		} );

	// Complex values optimization
	Environment.newSystemFunction( "#op*", ( Environment env, PositionData posData, ComplexValue a, RealValue b ) {
			enforce( b.value != 0, new EvaluationException( "divisionByZero", posData ) );

			return complexValue( a / b );
		} );
}