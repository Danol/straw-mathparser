﻿module straw.mathParser.func.basic.numberOps;

import straw.mathParser.func.toolkit;

/// Absolute value
shared static this() {
	Environment.newSystemFunction( "abs", ( RealValue a ) {
			return a.value >= 0 ? a : realValue( -a.value );
		} );
	Environment.newSystemFunction( "abs", ( Environment env, PositionData posData, ComplexValue a ) {
			return realValue( a.value.abs );
		} );
}

/// Complex special
shared static this() {
	Environment.newSystemVariable( "i", complexValue( 0, 1 ) );

	// Conjuged number
	Environment.newSystemFunction( "conj", ( Environment env, PositionData posData, ComplexValue a ) {
			return a.value.conj.complexValue;
		} );

	// Argument/phase
	Environment.newSystemFunction( "arg", ( Environment env, PositionData posData, ComplexValue a ) {
			return a.value.arg.realValue;
		} );
}