﻿module dllMain;
version( DLL ):

import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.string;
import straw.mathParser.env;
import straw.mathParser.grammar.entry;
import straw.mathParser.value;
import straw.parser;

alias CString = const( char )*;

extern( C++ )
interface DLLInterface {

public:
	CString getResult();
	CString getError();
	CString getErrorArg();

	size_t getErrorPos();
	size_t getErrorPosLength();

public:
	void setMaxNesting( uint set );
	void setUseFractions( bool set );
	void setDenominatorLimit( uint set );

public:
	void parse( CString expr, bool intermediate, uint timeout );

	// Does not parse anything, just 'redisplays' last result - useful when display settings has changed
	void redisplay();

}

private DLLInterface iface;

export extern( C )
DLLInterface getInterface() {
	if( !iface )
		iface = new DLLInterfaceImpl;

	return iface;
}


final class DLLInterfaceImpl : DLLInterface {

private:
	Environment env;
	Parser parser;
	Values lastResultValues;

	CString resultStr, errorStr, errorArgStr;
	size_t errorPos, errorPosLength;

	Value.Sink resultBuffer;

public:
	this() {
		env = new Environment();
		parser = new Parser();

		resultBuffer = appender!( char[] )();
	}

	extern( C++ ) {

	public:
		override CString getResult() {
			return resultStr;
		}
		override CString getError() {
			return errorStr;
		}
		override CString getErrorArg() {
			return errorArgStr;
		}

		override size_t getErrorPos() {
			return errorPos;
		}
		override size_t getErrorPosLength() {
			return errorPosLength;
		}

	public:
		void setMaxNesting( uint set ) {
			env.maxNesting = set;
		}
		void setUseFractions( bool set ) {
			env.useFractions = set;
		}
		void setDenominatorLimit( uint set ) {
			env.denominatorLimit = set;
		}

	public:
		override void parse( CString expr, bool intermediate, uint timeout ) {
			// Create temporary namespace if the mode is intermediate
			if( intermediate )
				env.namespaceStack ~= Namespace();
				
			string error, errorArg;
			errorPos = -1;

			env.timeout = timeout;
			env.start();

			try {
				parser.reuse( expr.to!string );
				Symbol_Entry parsed = parser.parse!Symbol_Entry();

				enforce( parsed, new SyntaxException( "unexpectedSymbol", PositionData( 0, 0, 0, parser.sourceLen ) ) );
				enforce( parser.getChar == 0, new SyntaxException( "unexpectedSymbol", PositionData( parser.posData, parser.sourceLen - parser.pos ) ) );

				lastResultValues = parsed.calculate( env );
				redisplay();
			}
			catch( ParserException e ) {
				error = e.msg;
				errorArg = e.arg;
				errorPos = e.posData.posInFile;
				errorPosLength = e.posData.length;
			}
			catch( Throwable e ) {
				error = "##EXCEPTION##:" ~ e.msg;
				errorPos = 0;
			}

			errorStr = error.toStringz;
			errorArgStr = errorArg.toStringz;

			if( intermediate )
				env.namespaceStack.length --;

			else if( lastResultValues.length )
				env.defVariable( "ans", lastResultValues[ $-1 ], PositionData() );
		}
		override void redisplay() {
			resultBuffer.clear();

			if( lastResultValues.length ) {
				lastResultValues[ 0 ].toString( env, resultBuffer );

				foreach( val; lastResultValues[ 1 .. $ ] ) {
					resultBuffer ~= ", ";
					val.toString( env, resultBuffer );
				}
			}

			resultBuffer ~= cast( char ) 0;
			resultStr = resultBuffer.data.ptr;
		}

	}

}