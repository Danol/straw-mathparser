﻿module straw.mathParser.value.value;

import std.algorithm;
import std.array;
import straw.parser.parser;
import straw.mathParser.env;

alias Values = Value[];

enum MaxValueLevel = 7;

alias Casts = ushort[ MaxValueLevel ];
enum Casts errorCasts = [0].replicate( MaxValueLevel );

abstract class Value {

public:
	enum sname = "any";
	enum slevel = MaxValueLevel;

public:
	abstract @property string name();
	abstract @property ushort level();
	abstract @property string[] castableTo();

public:
	/// Casts a value to another value type; the type is determined by the targetName (Value type's name/sname)
	Value castTo( string name ) {
		if( name == this.name )
			return this;

		else if( name == "any" )
			return this;

		else
			return castToImpl( name );
	}
	T castTo( T )()
		if( is( T : Value ) )
	{
		return cast( T ) castTo( T.sname );
	}

	abstract Value castToImpl( string targetName );

public:
	final override string toString() {
		assert( 0 );
	}
	abstract string toString( Environment env );

}

mixin template Value_mix( string sname_, ushort slevel_, Castables ... ) {

public:
	enum sname = sname_;
	enum slevel = slevel_;
	enum scastableTo = {
		string[] result;
		foreach( c; Castables ) {
			result ~= c.sname;
			static assert( c.slevel > slevel );
		}
		return result;
	} ();

public:
	override @property string name() {
		return sname;
	}
	override @property ushort level() {
		return slevel;
	}
	override @property string[] castableTo() {
		return scastableTo;
	}

}