﻿module straw.mathParser.value;
public:

import straw.mathParser.value.any;
import straw.mathParser.value.complex;
import straw.mathParser.value.integral;
import straw.mathParser.value.rationalWithSqrt;
import straw.mathParser.value.realv;
import straw.mathParser.value.value;

