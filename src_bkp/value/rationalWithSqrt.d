﻿module straw.mathParser.value.rationalWithSqrt;

import std.algorithm;
import std.conv;
import std.exception;
import std.math;
import std.numeric;
import straw.mathParser.exception;
import straw.mathParser.value;
import straw.parser.parser;
import straw.mathParser.env;

Value rationalWithSqrtValue( long numerator, long denominator = 1, long numerator_sqrt = 1, bool negative = false ) {
	auto result = RationalWithSqrt( numerator, denominator, numerator_sqrt, negative );

	if( result.denominator == 1 && result.numerator_sqrt == 1 )
		return new IntegralValue( result.numerator );

	else
		return new RationalWithSqrtValue( result );
}
RationalWithSqrtValue safeRationalWithSqrtValue( long numerator, long denominator = 1, long numerator_sqrt = 1, bool negative = false ) {
	return new RationalWithSqrtValue( RationalWithSqrt( numerator, denominator, numerator_sqrt, negative, false ) );
}

final class RationalWithSqrtValue : Value {
	mixin Value_mix!( "rationalWithSqrt", 1, RealValue, AnyNumericalNonComplexValue, ComplexValue, AnyNumericalValue );

public:
	RationalWithSqrt value;
	alias value this;

public:
	this( RationalWithSqrt val ) {
		this.value = val;
	}

public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {

			case RealValue.sname:
				return new RealValue( toReal );

			case AnyNumericalNonComplexValue.sname:
				return new AnyNumericalNonComplexValue( this );

			case ComplexValue.sname:
				return new ComplexValue( this, Integral_zero );

			case AnyNumericalValue.sname:
				return new AnyNumericalValue( this );

			default:
				assert( 0 );

		}
	}

public:
	override string toString( Environment env ) {
		if( env.showRWSInDecimal )
			return ( cast( real ) value.numerator / cast( real ) value.denominator * sqrt( cast( real ) value.numerator_sqrt ) ).to!string;
		else
			return ( value.negative ? "-" : "" ) ~ ( value.numerator == 1 && value.numerator_sqrt != 1 ? "" : value.numerator.to!string ) ~ ( value.numerator_sqrt != 1 ? "√" ~ value.numerator_sqrt.to!string : "" ) ~ ( value.denominator != 1 ? "/" ~ value.denominator.to!string : "" );
	}
	
}

struct RationalWithSqrt {
	
public:
	long numerator = 1, denominator = 1, numerator_sqrt = 1;
	bool negative = false;
	
public:
	this( long numerator, long denominator = 1, long numerator_sqrt = 1, bool negative = false, bool doRationalize = true ) {
		this.numerator = abs( numerator );
		this.denominator = abs( denominator );
		this.numerator_sqrt = abs( numerator_sqrt );
		this.negative = negative;

		if( doRationalize )
			rationalize();
	}
	
private:
	void rationalize() {
		// Sign
		if( numerator < 0 )
			numerator *= -1, negative = !negative;
		
		if( denominator < 0 )
			denominator *= -1, negative = !negative;
		
		if( numerator_sqrt < 0 )
			numerator_sqrt *= -1, negative = !negative;
		
		// Simplify sqrt
		for( long a = cast( long ) ( cast( real ) numerator_sqrt ).sqrt.ceil; a > 1; a -- ) {
			if( numerator_sqrt % ( a * a ) == 0 ) {
				numerator *= a;
				numerator_sqrt /= a * a;
				break;
			}
		}
		
		// Simplify fraction
		long gcd_ = gcd( numerator, denominator );
		numerator /= gcd_;
		denominator /= gcd_;
		
		if( numerator_sqrt == 0 ) {
			numerator = 0;
			numerator_sqrt = 1;
			denominator = 1;
			
		} else if( numerator == 0 ) {
			numerator_sqrt = 1;
			denominator = 1;
		}
	}
	
public:
	real toReal() @safe {
		return cast( real ) numerator * sqrt( cast( real ) numerator_sqrt ) / cast( real ) denominator * ( negative ? -1.0 : 1.0 );
	}
	
}

__gshared {
	auto RWS_sqrt3 = safeRationalWithSqrtValue( 1, 1, 3, false );

	auto RWS_one_half = safeRationalWithSqrtValue( 1, 2, 1, false );
	auto RWS_sqrt2_half = safeRationalWithSqrtValue( 1, 2, 2, false );
	auto RWS_sqrt3_half = safeRationalWithSqrtValue( 1, 2, 3, false );

	auto RWS_sqrt3_third = safeRationalWithSqrtValue( 1, 2, 3, false );

	auto RWS_minus_sqrt3 = safeRationalWithSqrtValue( 1, 1, 3, true );

	auto RWS_minus_one_half = safeRationalWithSqrtValue( 1, 2, 1, true );
	auto RWS_minus_sqrt2_half = safeRationalWithSqrtValue( 1, 2, 2, true );
	auto RWS_minus_sqrt3_half = safeRationalWithSqrtValue( 1, 2, 3, true );

	auto RWS_minus_sqrt3_third = safeRationalWithSqrtValue( 1, 2, 3, true );
}