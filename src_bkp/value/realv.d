﻿module straw.mathParser.value.realv;

import std.algorithm;
import std.conv;
import std.math;
import straw.mathParser.value;
import straw.mathParser.env;

Value realValue( real value ) {
	if( value == 0 )
		return Integral_zero;

	if( value.floor == value )
		return new IntegralValue( cast( long ) value );

	return new RealValue( value );
}

final class RealValue : Value {
	mixin Value_mix!( "real", 2, ComplexValue, AnyNumericalNonComplexValue, AnyNumericalValue );

public:
	real value;
	alias value this;
	
public:
	this( real value ) {
		this.value = value;
	}
	
public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {

			case ComplexValue.sname:
				return new ComplexValue( this, Integral_zero );

			case AnyNumericalNonComplexValue.sname:
				return new AnyNumericalNonComplexValue( this );

			case AnyNumericalValue.sname:
				return new AnyNumericalValue( this );

			default:
				assert( 0 );

		}
	}
	
public:
	override string toString( Environment env ) {
		return value.to!string;
	}

}

__gshared {
	auto Real_degPerRad = new RealValue( 180.0 / PI );
	auto Real_radPerDeg = new RealValue( PI / 180.0 );

	auto Real_ln2 = new RealValue( LN2 );
	auto Real_ln10 = new RealValue( LN10 );

	auto Real_pi = new RealValue( PI );
	auto Real_e = new RealValue( E );
}