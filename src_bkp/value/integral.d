﻿module straw.mathParser.value.integral;

import std.conv;
import std.algorithm;
import straw.mathParser.value;
import straw.mathParser.env;

Value integralValue( long value ) {
	return new IntegralValue( value );
}

final class IntegralValue : Value {
	mixin Value_mix!( "integral", 0, RationalWithSqrtValue, RealValue, AnyNumericalNonComplexValue, ComplexValue, AnyNumericalValue );

public:
	long value;
	alias value this;
	
public:
	this( long value ) {
		this.value = value;
	}
	
public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {

			case RationalWithSqrtValue.sname:
				return new RationalWithSqrtValue( RationalWithSqrt( value ) );

			case RealValue.sname:
				return new RealValue( value );

			case AnyNumericalNonComplexValue.sname:
				return new AnyNumericalNonComplexValue( this );
				
			case ComplexValue.sname:
				return new ComplexValue( this, Integral_zero );

			case AnyNumericalValue.sname:
				return new AnyNumericalValue( this );
				
			default:
				assert( 0 );
				
		}
	}
	
public:
	override string toString( Environment env ) {
		return value.to!string;
	}
		
}

__gshared {
	auto Integral_zero = new IntegralValue( 0 );

	auto Integral_one = new IntegralValue( 1 );
	auto Integral_two = new IntegralValue( 2 );
	auto Integral_three = new IntegralValue( 3 );

	auto Integral_minus_one = new IntegralValue( -1 );
	auto Integral_minus_two = new IntegralValue( -2 );
	auto Integral_minus_three = new IntegralValue( -3 );
}