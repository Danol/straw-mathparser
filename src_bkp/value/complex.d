﻿module straw.mathParser.value.complex;

import std.algorithm;
import std.conv;
import std.math;
import straw.mathParser.value;
import straw.mathParser.env;

Value complexValue( Value realPart, Value imaginaryPart ) {
	IntegralValue val = cast( IntegralValue ) imaginaryPart;

	if( val && val.value == 0 )
		return realPart;

	else
		return new ComplexValue( realPart, imaginaryPart );
}

final class ComplexValue : Value {
	mixin Value_mix!( "complex", 4, AnyNumericalValue );
	
public:
	Value realPart, imaginaryPart;
	
public:
	this( Value realPart, Value imaginaryPart ) {
		this.realPart = realPart;
		this.imaginaryPart = imaginaryPart;
	}

public:
	override Value castToImpl( string targetName ) {
		switch( targetName ) {
			
			case AnyNumericalValue.sname:
				return new AnyNumericalValue( this );
				
			default:
				assert( 0 );
				
		}
	}

public:
	override string toString( Environment env ) {
		IntegralValue val = cast( IntegralValue ) realPart;
		if( val && val.value == 0 )
			return imaginaryPart.toString( env ) ~ "i";
		else
			return realPart.toString( env ) ~ " + " ~ imaginaryPart.toString( env ) ~ "i";
	}
	
}