﻿module straw.mathParser.func.statistic;

import straw.mathParser.func.toolkit;

shared static this() {
	Environment.newSystemFunction( "avg", ( Environment env, PositionData posData, AnyNumericalValue arg1, AnyNumericalValue[] args ) {
		Value sum = arg1.value;
		
		foreach( arg; args ) 
			sum = env.calculateIdentifier( "#op+", [ sum, arg.value ], posData );
		
		return env.calculateIdentifier( "#op/", [ sum, new IntegralValue( args.length + 1 ) ], posData );
	} );
}