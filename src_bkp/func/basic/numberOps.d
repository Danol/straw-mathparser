﻿module straw.mathParser.func.basic.numberOps;

import std.math;
import std.numeric;
import straw.mathParser.func.toolkit;

/// Absolute value
shared static this() {
	Environment.newSystemFunction( "abs", ( IntegralValue a ) {
			return a.value >= 0 ? a : new IntegralValue( -a.value );
		} );
	Environment.newSystemFunction( "abs", ( RationalWithSqrtValue a ) {
			if( !a.negative )
				return a;
			
			auto val = a.value;
			a.negative = !a.negative;
			return new RationalWithSqrtValue( val );
		} );
	Environment.newSystemFunction( "abs", ( RealValue a ) {
			return a.value >= 0 ? a : new RealValue( -a.value );
		} );
	Environment.newSystemFunction( "abs", ( Environment env, PositionData posData, ComplexValue a ) {
			return env.calculateIdentifier( "sqrt", [
					env.calculateIdentifier( "#op+", [
							env.calculateIdentifier( "sqr", [ a.realPart ], posData ),
							env.calculateIdentifier( "sqr", [ a.imaginaryPart ], posData )
						], posData )
				], posData );
		} );
}

/// Complex special
shared static this() {
	Environment.newSystemVariable( "i", new ComplexValue( Integral_zero, Integral_one ) );

	// Conjuged number
	Environment.newSystemFunction( "conj", ( Environment env, PositionData posData, ComplexValue a ) {
			return complexValue( a.realPart, env.calculateIdentifier( "#opUnary-", [ a.imaginaryPart ], posData ) );
		} );

	// Argument/phase
	Environment.newSystemFunction( "arg", ( Environment env, PositionData posData, ComplexValue a ) {
			return realValue( atan2( a.imaginaryPart.castTo!RealValue.value, a.realPart.castTo!RealValue.value ) );
		} );
}