﻿module straw.mathParser.func.basic.additive;

import std.math;
import std.numeric;
import straw.mathParser.func.toolkit;

/// Plus
shared static this() {
	Environment.newSystemFunction( "#op+", ( IntegralValue a, IntegralValue b ) {
			return integralValue( a + b );
		} );
	Environment.newSystemFunction( "#op+", ( RationalWithSqrtValue a, RationalWithSqrtValue b ) {
			if( a.numerator_sqrt != b.numerator_sqrt )
				return realValue( a.toReal + b.toReal );

			if( a.denominator == b.denominator )
				return rationalWithSqrtValue( a.numerator * ( a.negative ? -1 : 1 ) + b.numerator * ( b.negative ? -1 : 1 ), a.denominator, a.numerator_sqrt );

			return rationalWithSqrtValue(
				a.numerator * b.denominator * ( a.negative ? -1 : 1 ) + b.numerator * a.denominator * ( b.negative ? -1 : 1 ),
				a.denominator * b.denominator,
				a.numerator_sqrt
				);
		} );
	Environment.newSystemFunction( "#op+", ( RealValue a, RealValue b ) {
			return realValue( a + b );
		} );
	Environment.newSystemFunction( "#op+", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			return complexValue(
				env.calculateIdentifier( "#op+", [ a.realPart, b.realPart ], posData ),
				env.calculateIdentifier( "#op+", [ a.imaginaryPart, b.imaginaryPart ], posData ),
				);
		} );
}

/// Minus
shared static this() {
	Environment.newSystemFunction( "#op-", ( IntegralValue a, IntegralValue b ) {
			return integralValue( a - b );
		} );
	Environment.newSystemFunction( "#op-", ( RationalWithSqrtValue a, RationalWithSqrtValue b ) {
			if( a.numerator_sqrt != b.numerator_sqrt )
				return realValue( a.toReal + b.toReal );

			if( a.denominator == b.denominator )
				return rationalWithSqrtValue( a.numerator * ( a.negative ? -1 : 1 ) - b.numerator * ( b.negative ? -1 : 1 ), a.denominator, a.numerator_sqrt );
			
			return rationalWithSqrtValue(
				a.numerator * b.denominator * ( a.negative ? -1 : 1 ) - b.numerator * a.denominator * ( b.negative ? -1 : 1 ),
				a.denominator * b.denominator,
				a.numerator_sqrt
				);
		} );
	Environment.newSystemFunction( "#op-", ( RealValue a, RealValue b ) {
			return realValue( a - b );
		} );
	Environment.newSystemFunction( "#op-", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			return complexValue(
				env.calculateIdentifier( "#op-", [ a.realPart, b.realPart ], posData ),
				env.calculateIdentifier( "#op-", [ a.imaginaryPart, b.imaginaryPart ], posData ),
				);
		} );
}

/// Unary minus
shared static this() {
	Environment.newSystemFunction( "#opUnary-", ( IntegralValue a ) {
			return integralValue( -a );
		} );
	Environment.newSystemFunction( "#opUnary-", ( RationalWithSqrtValue a ) {
			return rationalWithSqrtValue( a.numerator, a.denominator, a.numerator_sqrt, !a.negative );
		} );
	Environment.newSystemFunction( "#opUnary-", ( RealValue a ) {
			return realValue( -a );
		} );
	Environment.newSystemFunction( "#opUnary-", ( Environment env, PositionData posData, ComplexValue a ) {
			return complexValue( env.calculateIdentifier( "#opUnary-", [ a.realPart ], posData ), env.calculateIdentifier( "#opUnary-", [ a.imaginaryPart ], posData ) );
		} );

}