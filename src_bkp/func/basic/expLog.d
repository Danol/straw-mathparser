﻿module straw.mathParser.func.basic.expLog;

import std.math;
import std.numeric;
import straw.mathParser.func.toolkit;

/// Power
shared static this() {
	// Wrapper
	Environment.newSystemFunction( "pow", ( Environment env, PositionData posData, AnyNumericalValue a, AnyNumericalValue b ) {
			return env.calculateIdentifier( "#op^", [ a.value, b.value ], posData );
		} );

	Environment.newSystemFunction( "#op^", ( IntegralValue a, IntegralValue b ) {
			if( b.value >= 0 )
				return integralValue( pow( a.value, b.value ) );
			
			else
				return rationalWithSqrtValue( 1, pow( a.value, -b.value ) );
		} );
	Environment.newSystemFunction( "#op^", ( RationalWithSqrtValue a, IntegralValue b ) {
			if( b.value == 0 )
				return cast( Value ) Integral_one;
			
			else if( b.value > 0 ) {
				return rationalWithSqrtValue(
					pow( a.numerator, b.value ) * pow( a.numerator_sqrt, b.value / 2 ),
					pow( a.denominator, b.value ),
					b % 2 ? a.numerator_sqrt : 1,
					a.negative && b % 2
					);
			}
			
			else /* if( b.value < 0 ) */ {
				return rationalWithSqrtValue( 
					pow( a.denominator, b.value ),
					pow( a.numerator, b.value ) * pow( a.numerator_sqrt, b / 2 + b % 2 ),
					b % 2 ? a.numerator_sqrt : 1,
					a.negative && b % 2
					);
			}
		} );
	Environment.newSystemFunction( "#op^", ( Environment env, PositionData posData, RealValue a, RealValue b ) {
			if( a.value == 0 )
				return Integral_one;
			
			// (-3)^x - needs complex result
			if( a.value < 0 )
				return env.calculateIdentifier( "#op^", [ a.castTo!ComplexValue, b ], posData );
			
			return realValue( pow( a.value, b.value ) );
		} );
	Environment.newSystemFunction( "#op^", ( Environment env, PositionData posData, ComplexValue a, RealValue b ) {
			if( b.value == 0 )
				return cast( Value ) Integral_one;
			
			if( b.value == 1 )
				return a;
			
			if( b.value == -1 )
				return env.calculateIdentifier( "#op/", [ Integral_one, a ], posData );
			
			if( b.value == 2 )
				return env.calculateIdentifier( "#op*", [ a, a ], posData );
			
			if( b.value == -2 )
				return env.calculateIdentifier( "#op/", [ Integral_one, env.calculateIdentifier( "#op*", [ a, a ], posData ) ], posData );

			real aIm = a.imaginaryPart.castTo!RealValue.value;
			real aRe = a.realPart.castTo!RealValue.value;

			real angle = atan2( aIm, aRe ) * b.value;
			real expd = pow( sqrt( aIm * aIm + aRe * aRe ), b.value );

			return complexValue( realValue( expd * cos( angle ) ), realValue( expd * sin( angle ) ) );
		} );
	Environment.newSystemFunction( "#op^", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			real aIm = a.imaginaryPart.castTo!RealValue.value;
			real aRe = a.realPart.castTo!RealValue.value;
			
			real bIm = b.imaginaryPart.castTo!RealValue.value;
			real bRe = b.realPart.castTo!RealValue.value;

			real aAbs = sqrt( aIm * aIm + aRe * aRe );
			real aArg = atan2( aIm, aRe );

			real mult = pow( aAbs, bRe ) * exp( -bIm * aArg );
			real angle = bIm * log( aAbs ) + bRe * aArg;

			return complexValue( realValue( mult * cos( angle ) ), realValue( mult * sin( angle ) ) );
		} );
}

/// Square
shared static this() {
	Environment.newSystemFunction( "sqr", ( IntegralValue a ) {
			return integralValue( a * a );
		} );
	Environment.newSystemFunction( "sqr", ( RationalWithSqrtValue a ) {
			return rationalWithSqrtValue(
				a.numerator * a.numerator * a.numerator_sqrt,
				a.denominator * a.denominator,
				1,
				false
				);
		} );
	Environment.newSystemFunction( "sqr", ( RealValue a ) {
			return realValue( a * a );
		} );
	Environment.newSystemFunction( "sqr", ( Environment env, PositionData posData, ComplexValue a ) {
			return env.calculateIdentifier( "#op*", [ a, a ], posData );
		} );
}

/// Square root
shared static this() {
	Environment.newSystemFunction( "sqrt", ( IntegralValue a ) {
			if( a.value < 0 )
				return new ComplexValue( Integral_zero, rationalWithSqrtValue( 1, 1, -a ) );
			else
				return rationalWithSqrtValue( 1, 1, a );
		} );
	Environment.newSystemFunction( "sqrt", ( Environment env, PositionData posData, RationalWithSqrtValue a ) {
			if( a.numerator_sqrt != 1 )
				return env.calculateIdentifier( "sqrt", [ a.castTo!RealValue ], posData );

			auto result = rationalWithSqrtValue( 1, a.denominator, a.numerator * a.denominator, false );

			if( a.negative )
				return complexValue( Integral_zero, result );
			else
				return result;
		} );
	Environment.newSystemFunction( "sqrt", ( RealValue a ) {
			if( a.value < 0 )
				return complexValue( Integral_zero, realValue( sqrt( -a.value ) ) );
			else
				return realValue( sqrt( a.value ) );
		} );
	Environment.newSystemFunction( "sqrt", ( Environment env, PositionData posData, ComplexValue a ) {
			return env.calculateIdentifier( "#op^", [ a, realValue( 0.5 ) ], posData );
		} );
}

/// Natural logarithm (ln)
shared static this() {
	Environment.newSystemFunction( "ln", ( RealValue a ) {
			return realValue( log( a ) );
		} );
	Environment.newSystemFunction( "ln", ( Environment env, PositionData posData, ComplexValue a ) {
			return complexValue(
				env.calculateIdentifier( "ln", [ env.calculateIdentifier( "abs", [ a ], posData ) ], posData ),
				realValue( atan2( a.imaginaryPart.castTo!RealValue, a.realPart.castTo!RealValue ) )
				);
		} );

	Environment.newSystemFunction( "ln", ( Environment env, PositionData posData, AnyNumericalValue a, RealValue b ) {
			return env.calculateIdentifier( "#op/", [
					env.calculateIdentifier( "ln", [ a ], posData ),
					realValue( ln( b.value ) )
				], posData );
		} );
	Environment.newSystemFunction( "ln2", ( Environment env, PositionData posData, AnyNumericalValue a, RealValue b ) {
			return env.calculateIdentifier( "#op/", [
					env.calculateIdentifier( "ln", [ a ], posData ),
					Real_ln2
				], posData );
		} );
	Environment.newSystemFunction( "ln10", ( Environment env, PositionData posData, AnyNumericalValue a, RealValue b ) {
			return env.calculateIdentifier( "#op/", [
					env.calculateIdentifier( "ln", [ a ], posData ),
					Real_ln10
				], posData );
		} );
}

/// Decadic logarithm (log)
shared static this() {
	Environment.newSystemFunction( "log", ( RealValue a ) {
			return realValue( log10( a ) );
		} );
	Environment.newSystemFunction( "log", ( Environment env, PositionData posData, ComplexValue a ) {
			return env.calculateIdentifier( "#op/", [ env.calculateIdentifier( "ln", [ a ], posData ), Real_ln10 ], posData );
		} );
}

/// Euler stuff
shared static this() {
	Environment.newSystemVariable( "e", Real_e );

	Environment.newSystemFunction( "exp", ( Environment env, PositionData posData, AnyNumericalValue a ) {
			return env.calculateIdentifier( "#op^", [ Real_e, a.value ], posData );
		} );
}