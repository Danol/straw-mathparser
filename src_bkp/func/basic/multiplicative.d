﻿module straw.mathParser.func.basic.multiplicative;

import std.math;
import std.numeric;
import straw.mathParser.func.toolkit;

/// Multiply
shared static this() {
	Environment.newSystemFunction( "#op*", ( IntegralValue a, IntegralValue b ) {
			return integralValue( a * b );
		} );
	Environment.newSystemFunction( "#op*", ( RationalWithSqrtValue a, RationalWithSqrtValue b ) {
			return rationalWithSqrtValue(
				a.numerator * b.numerator,
				a.denominator * b.denominator,
				a.numerator_sqrt * b.numerator_sqrt,
				a.negative != b.negative
				);
		} );
	Environment.newSystemFunction( "#op*", ( RealValue a, RealValue b ) {
			return realValue( a * b );
		} );
	Environment.newSystemFunction( "#op*", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			return complexValue(
				env.calculateIdentifier( "#op-", [
						env.calculateIdentifier( "#op*", [ a.realPart, b.realPart ], posData ),
						env.calculateIdentifier( "#op*", [ a.imaginaryPart, b.imaginaryPart ], posData )
					], posData ),
				
				env.calculateIdentifier( "#op+", [
						env.calculateIdentifier( "#op*", [ a.realPart, b.imaginaryPart ], posData ),
						env.calculateIdentifier( "#op*", [ a.imaginaryPart, b.realPart ], posData )
					], posData ),
				);
		} );

	// Complex values optimization
	Environment.newSystemFunction( "#op*", ( Environment env, PositionData posData, ComplexValue a, AnyNumericalNonComplexValue b ) {
			return complexValue(
				env.calculateIdentifier( "#op*", [ a.realPart, b.value ], posData ),
				env.calculateIdentifier( "#op*", [ a.imaginaryPart, b.value ], posData )
				);
		} );
	Environment.newSystemFunction( "#op*", ( Environment env, PositionData posData, AnyNumericalNonComplexValue a, ComplexValue b ) {
			return complexValue(
				env.calculateIdentifier( "#op*", [ a.value, b.realPart ], posData ),
				env.calculateIdentifier( "#op*", [ a.value, b.imaginaryPart ], posData )
				);
		} );
}

/// Divide
shared static this() {
	Environment.newSystemFunction( "#op/", ( PositionData posData, IntegralValue a, IntegralValue b ) {
			enforce( b.value != 0, new EvaluationException( "divisionByZero", posData ) );
			
			if( a % b ) {
				return rationalWithSqrtValue( a, b );
			}
			
			return integralValue( a / b );
		} );
	Environment.newSystemFunction( "#op/", ( PositionData posData, RationalWithSqrtValue a, RationalWithSqrtValue b ) {
			enforce( b.numerator != 0, new EvaluationException( "divisionByZero", posData ) );
			
			return rationalWithSqrtValue(
				a.numerator * b.denominator,
				a.denominator * b.numerator * b.numerator_sqrt,
				a.numerator_sqrt * b.numerator_sqrt,
				a.negative != b.negative
				);
		} );
	Environment.newSystemFunction( "#op/", ( PositionData posData, RealValue a, RealValue b ) {
			enforce( b.value != 0, new EvaluationException( "divisionByZero", posData ) );
			return realValue( a / b );
		} );
	Environment.newSystemFunction( "#op/", ( Environment env, PositionData posData, ComplexValue a, ComplexValue b ) {
			return
				env.calculateIdentifier( "#op/", [
						env.calculateIdentifier( "#op*", [
								a,
								env.calculateIdentifier( "conj", [ b ], posData )
							], posData ),
						env.calculateIdentifier( "abs", [ b ], posData )
					], posData );
		} );

	// Complex optimization
	Environment.newSystemFunction( "#op/", ( Environment env, PositionData posData, ComplexValue a, AnyNumericalNonComplexValue b ) {
			return complexValue(
				env.calculateIdentifier( "#op/", [ a.realPart, b.value ], posData ),
				env.calculateIdentifier( "#op/", [ a.imaginaryPart, b.value ], posData )
				);
		} );
}