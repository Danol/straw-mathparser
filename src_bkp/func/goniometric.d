﻿module straw.mathParser.func.goniometric;

import std.math;
import straw.mathParser.func.toolkit;

// pi
shared static this() {
	Environment.newSystemVariable( "pi", Real_pi );
}

// sin, cos, tan, cot
shared static this() {
	Environment.newSystemFunction( "sin", ( Environment env, RealValue a ) {
			real angle = env.envToRad( a ) % ( 2 * PI );

			if( angle < 0 )
				angle += 2*PI;
				
			return sinTable.get( angle, realValue( sin( angle ) )  );
		} );
	Environment.newSystemFunction( "cos", ( Environment env, RealValue a ) {
			real angle = env.envToRad( a + PI/2 ) % ( 2 * PI );
			
			if( angle < 0 )
				angle += 2*PI;
			
			return sinTable.get( angle, realValue( sin( angle ) )  );
		} );

	Environment.newSystemFunction( "tan", ( Environment env, RealValue a ) {
			real angle = env.envToRad( a ) % PI;
			
			if( angle < 0 )
				angle += 2*PI;
			
			return tgTable.get( angle, realValue( tan( angle ) )  );
		} );
	Environment.newSystemFunction( "cot", ( Environment env, RealValue a ) {
			real angle = env.envToRad( a ) % PI;
			
			if( angle < 0 )
				angle += 2*PI;
			
			return cotgTable.get( angle, realValue( 1 / tan( angle ) )  );
		} );
}

// asin, acos, atan, acot
shared static this() {
	Environment.newSystemFunction( "asin", ( Environment env, RealValue a ) {
			return env.radToEnv( asin( a.value ) );
		} );
	Environment.newSystemFunction( "acos", ( Environment env, RealValue a ) {
			return env.radToEnv( acos( a.value ) );
		} );
	Environment.newSystemFunction( "atan", ( Environment env, RealValue a ) {
			return env.radToEnv( atan( a.value ) );
		} );
	Environment.newSystemFunction( "acot", ( Environment env, RealValue a ) {
			return env.radToEnv( atan( 1 / a.value ) );
		} );
}

// Tables
__gshared Value[ real ] sinTable, tgTable, cotgTable;

shared static this() {
	sinTable = [
		0 			: Integral_zero,
		PI/6		: RWS_one_half,
		PI/4		: RWS_sqrt2_half,
		PI/3		: RWS_sqrt3_half,
		PI/2		: Integral_one,
		PI*2/3	: RWS_sqrt3_half,
		PI*3/4	: RWS_sqrt2_half,
		PI*5/6	:	RWS_one_half,
		PI			: Integral_zero,
		PI*7/6	: RWS_minus_one_half,
		PI*5/4	: RWS_minus_sqrt2_half,
		PI*4/3	: RWS_minus_sqrt3_half,
		PI*3/2	: Integral_minus_one,
		PI*5/3	: RWS_minus_sqrt3_half,
		PI*7/4	: RWS_minus_sqrt2_half,
		PI*11/6	: RWS_minus_one_half
	];
	tgTable = [
		0 			: Integral_zero,
		PI/6		: RWS_sqrt3_third,
		PI/4		: Integral_one,
		PI/3		: RWS_sqrt3,
		PI*2/3	: RWS_minus_sqrt3,
		PI*3/4	: Integral_minus_one,
		PI*5/6	:	RWS_minus_sqrt3_third,
	];
	cotgTable = [
		PI/6		: RWS_sqrt3,
		PI/4		: Integral_one,
		PI/3		: RWS_sqrt3_third,
		PI/2		: Integral_zero,
		PI*2/3	: RWS_minus_sqrt3_third,
		PI*3/4	: Integral_minus_one,
		PI*5/6	:	RWS_minus_sqrt3,
	];
}